import React, { Component } from 'react';
import { Root} from "native-base";
import { LogBox, Alert,Platform,BackHandler } from 'react-native';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from './src/SplashScreen';

import { 
  LoginController,
  RegisterController,
  VerificationPhoneController,
  ForgotPasswordController,
  ForgotPasswordConfirmController 
} from './src';

import DrawerNavigator from './src/navigator/NavigatorContainer';

import store from './src/store';

const navOptionHandler = () => ({
  headerShown: false
})

const StackApp = createStackNavigator();

class App extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // LogBox.ignoreWarnings(['Animated: `useNativeDriver`']);
    LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
    LogBox.ignoreAllLogs();//Ignore all log notifications

    if (Platform.OS !== 'android') return

    BackHandler.addEventListener('hardwareBackPress', () => {
      //console.log('Close app');
      // const { dispatch } = this.props
      // dispatch({ type: 'Navigation/BACK' })
      Alert.alert("Exit", "Are you sure you want to close app?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
      ]);
      return true
    });
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress')
}

  render() {
    return (
      <Root>
        <Provider store={store}>
          <NavigationContainer>
            <StackApp.Navigator initialRouteName='SplashScreen'>
              <StackApp.Screen name="SplashScreen" component={SplashScreen} options={navOptionHandler} />
              <StackApp.Screen name="Login" component={LoginController} options={navOptionHandler} />
              <StackApp.Screen name="Register" component={RegisterController} options={navOptionHandler} />
              <StackApp.Screen name="ForgotPassword" component={ForgotPasswordController} options={navOptionHandler} />
              <StackApp.Screen name="ConfirmPassword" component={ForgotPasswordConfirmController} options={navOptionHandler} />
              <StackApp.Screen name="Verification" component={VerificationPhoneController} options={navOptionHandler} />
              <StackApp.Screen name="HomeApp" component={DrawerNavigator} options={navOptionHandler} />              
            </StackApp.Navigator>
          </NavigationContainer>
        </Provider>
      </Root>
    )
  }
}
export default App
