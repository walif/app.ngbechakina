const BRAND = {
    BRAND_001: require('./brand/brand_001.png'),
    BRAND_002: require('./brand/brand_002.png'),
    BRAND_003: require('./brand/brand_003.png'),
    BRAND_004: require('./brand/brand_004.png'),
    BRAND_005: require('./brand/brand_005.png'),
    BRAND_006: require('./brand/brand_006.png'),
}

export {BRAND}