const IMAGE = {
    DRAWER_HEADER_BG: require('./images/drawer_bg.jpg'),
    ICON_PROFILE: require('./images/my12class_logo.jpg'),
    AUTH_HEADER_BACKGROUND: require('./images/auth_header.png'),
    AUTH_HEADER_BACKGROUND: require('./images/auth_header.png'),
    APP_POWER_BY: require('./images/poweredby.png'),
    APP_DEVELOP_BY: require('./images/developerdby.png'),
    APP_LOGO: require('./images/ngbechakina_logo.png'),
    DEFAULT_PRODUCT: require('./images/default_product.jpg'),
}

export {IMAGE}