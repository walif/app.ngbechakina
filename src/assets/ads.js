const ADS = {
    IMAGE_THUMB_001: require('./advertisement/thumb/001.jpeg'),
    IMAGE_THUMB_002: require('./advertisement/thumb/002.jpeg'),
    IMAGE_THUMB_003: require('./advertisement/thumb/003.jpeg'),
    IMAGE_THUMB_004: require('./advertisement/thumb/004.jpg'),
    IMAGE_001: require('./advertisement/001.jpeg'),
    IMAGE_002: require('./advertisement/002.jpeg'),
    IMAGE_003: require('./advertisement/003.jpeg'),
    IMAGE_004: require('./advertisement/004.jpg'),
}

export { ADS }