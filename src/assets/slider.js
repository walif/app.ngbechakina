const SLIDER = {
    IMAGE_001: require('./slider/001.jpeg'),
    IMAGE_002: require('./slider/002.jpeg'),
    IMAGE_003: require('./slider/003.jpeg'),
    IMAGE_004: require('./slider/004.jpeg'),
}

export { SLIDER }