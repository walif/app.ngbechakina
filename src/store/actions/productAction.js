import Axios from 'axios';
import * as Types from './types';
import { SITE } from '.././../core';

//ONLY FOR AXIOS GET ERROR NOT SERVER
const dispatchError = (error, dispatch) => {    
    let res = {};
    if (!error.response) {
        res.errors = 'Network Error.';
        res.statusCode = 503;
    } else {
        res.statusCode = error.response.status
        res.errors = error.response.data
    }

    dispatch({
        type: Types.USERS_ERROR,
        payload: {
            errors: res.errors,
            status: false,
            statusCode: res.statusCode,
            timestamp: Date.now()
        }
    });
}

const requestInitial = (dispatch) => {
    dispatch({
        type: Types.REQUEST_PRODUCT        
    });
}


const responseSuccess = (response, actionType, dispatch) => {
    const { data, status, statusCode, timestamp,message } = response;
    dispatch({
        type: actionType,
        payload: {
            data,
            status,
            statusCode,
            timestamp,
            message
        }
    });
}

const responseError = (response, dispatch) => {
    //console.log('Error:',response);
    const { data, errors, status, statusCode, timestamp } = response;
    dispatch({
        type: Types.PRODUCT_RESPONSE_ERROR,
        payload: {
            data,
            errors,
            status,
            statusCode,
            timestamp
        }
    });
}

export const getProducts = (pageSize,pageNumber) => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Product/v1/GetApprovalClassifiedProducts/${pageSize}/${pageNumber}`)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_PRODUCTS, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getProductsByCategoryIdServer = (categoryId,pageSize,pageNumber) => dispatch => {
    Axios.get(`${SITE.apiMainUrl}api/Product/v1/GetApprovalClassifiedProductsByCategoryId/${categoryId}/${pageSize}/${pageNumber}`)
        .then(response => {
            const { errors } = response.data;
            //console.log('Action:',response);
            if (!errors) {
                const res = {
                    ...response.data,
                    actionFrom: 'server'
                };
                responseSuccess(res, Types.LOAD_PRODUCTS_BY_CATEGORY_ID, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getProductsByCustomerId = () => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Product/v1/GetClassifiedProductByCustomerId`)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_PRODUCTS_BY_CUSTOMER_ID, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getProductsByUserId = () => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Product/v1/GetClassifiedProductsByApprovalId`)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_PRODUCTS_BY_APPROVAL_ID, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getPendingProducts = () => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Product/v1/GetPendingClassifiedProducts`)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_PENDING_PRODUCTS, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getProductByProductId = (id) => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Product/v1/GetClassifiedProductDetailByProductId/` + id)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_PRODUCT_BY_PRODUCT_ID, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getAreas = () => dispatch => {
    
    requestInitial(dispatch);

    Axios.get(SITE.apiMainUrl + `api/Address/v1/GetAreas`)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_AREAS, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }

        })
        .catch(error => {
            console.log(error);
            //dispatchError(error, dispatch);
        });
}

export const getProductsByCategoryIdState = (categoryId) => dispatch => {
    const response = {
        data: { categoryId },
        statusCode: 200,
        status: true,
        timestamp: new Date()
    }
    responseSuccess(response, Types.LOAD_PRODUCTS_BY_CATEGORY_ID_STATE, dispatch);
}

export const saveProduct = (product) => dispatch => {

    //console.log('Axios:',product);
    requestInitial(dispatch);

    Axios.post(SITE.apiMainUrl + 'api/Product/v1/SaveClassifiedProduct', product)
        .then(response => {
            const { errors } = response.data.result;
            //console.log(response.data.result);
            if (!errors) {
                responseSuccess(response.data.result, Types.SAVE_PRODUCT, dispatch);
            }
            else {
                responseError(response.data.result, dispatch);
            }
        })
        .catch(error => {
            dispatchError(error, dispatch);
        });
}

export const approvedProduct = (product) => dispatch => {

    //console.log('Axios:',product);
    requestInitial(dispatch);

    Axios.post(SITE.apiMainUrl + 'api/Product/v1/ApprovedClassifiedProduct', product)
        .then(response => {
            const { errors } = response.data.result;
            //console.log(response.data.result);
            if (!errors) {
                responseSuccess(response.data.result, Types.APPROVED_PRODUCT, dispatch);
            }
            else {
                responseError(response.data.result, dispatch);
            }
        })
        .catch(error => {
            dispatchError(error, dispatch);
        });
}

export const cancelProduct = (product) => dispatch => {

    //console.log('Axios:',product);
    requestInitial(dispatch);

    Axios.post(SITE.apiMainUrl + 'api/Product/v1/CancelClassifiedProduct', product)
        .then(response => {
            const { errors } = response.data.result;
            //console.log(response.data.result);
            if (!errors) {
                responseSuccess(response.data.result, Types.CANCEL_PRODUCT, dispatch);
            }
            else {
                responseError(response.data.result, dispatch);
            }
        })
        .catch(error => {
            dispatchError(error, dispatch);
        });
}

export const closeProductModel = () => dispatch => {
    dispatch({
        type: Types.HIDE_PRODUCT_MODAL        
    });
}
