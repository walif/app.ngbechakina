﻿import Axios from 'axios';
import * as Types from './types';
import { SITE } from '../../core';

const requestInitial = (dispatch) => {
    dispatch({
        type: Types.REQUEST_CATEGORY        
    });
}

const responseSuccess = (response, actionType, dispatch) => {
    const { data, status, statusCode, timestamp } = response;
    dispatch({
        type: actionType,
        payload: {
            data,
            status,
            statusCode,
            timestamp
        }
    });
}

const responseError = (response, dispatch) => {
    const { data, errors, status, statusCode, timestamp } = response;
    dispatch({
        type: Types.CATEGORY_RESPONSE_ERROR,
        payload: {
            data,
            errors,
            status,
            statusCode,
            timestamp
        }
    });
}

export const loadCategories = () => dispatch => {
    
    requestInitial(dispatch);

    const uri = SITE.apiMainUrl + 'api/Category/v1/GetCategories';

    //console.log(uri);

    Axios.get(uri)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_CATEGORIES, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }
        })
        .catch(error => {
            console.log(error);
        });
}

export const loadSubCategoriesByCategoryId = (categoryId) => dispatch => {
    
    requestInitial(dispatch);
    
    Axios.get(SITE.apiMainUrl + 'api/Category/v1/GetSubCategoriesByCategoryId/' + categoryId)
        .then(response => {
            const { errors } = response.data;
            //console.log(response);
            if (!errors) {
                responseSuccess(response.data, Types.LOAD_CATEGORIES_BY_CATEGORY_ID, dispatch);
            }
            else {
                responseError(response.data, dispatch);
            }
        })
        .catch(error => {
            console.log(error);
        });
}

export const closeCategoryModel = () => dispatch => {
    dispatch({
        type: Types.HIDE_CATEGORY_MODAL        
    });
}

