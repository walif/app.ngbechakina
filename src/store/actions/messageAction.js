﻿import Axios from 'axios';
import * as Types from './types';
import { SITE } from '../../core';

export const loadMessages = () => dispatch => {
    Axios.get( SITE.apiMainUrl + 'api/Message/GetMessages')
        .then(response => {
            dispatch({
                type: Types.LOAD_MESSAGES,
                payload: {
                    messages: response.data
                }
            })
        })
        .catch(error => {
            console.log(error);
        });
}

export const loadMessageByMessageId = (messageId) => dispatch => {
    Axios.get(SITE.apiMainUrl + 'api/Message/GetMessageByMessageId/' + messageId)
        .then(response => {
            dispatch({
                type: Types.LOAD_MESSAGE_BY_MESSAGE_ID,
                payload: {
                    message: response.data
                }
            })
        })
        .catch(error => {
            console.log(error);
        });
}


