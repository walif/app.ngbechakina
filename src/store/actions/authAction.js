import Axios from 'axios';
import * as Types from './types';
import { SITE } from '.././../core';
import AuthHeaderToken from '../../utils/setAuthToken';

//ONLY FOR AXIOS GET ERROR NOT SERVER
const dispatchError = (error, dispatch) => {    
    let res = {};
    if (!error.response) {
        res.errors = 'Network Error.';
        res.statusCode = 503;
    } else {
        res.statusCode = error.response.status
        res.errors = error.response.data
    }

    dispatch({
        type: Types.USERS_ERROR,
        payload: {
            errors: res.errors,
            status: false,
            statusCode: res.statusCode,
            timestamp: Date.now()
        }
    });
}

const responseError = (errors,status,statusCode,timestamp, dispatch) => {

    dispatch({
        type: Types.USERS_ERROR,
        payload: {
            errors: errors,
            status: status,
            statusCode: statusCode,
            timestamp: timestamp
        }
    });
}

export const login = (user) => dispatch => {
    //console.log(user);
    Axios.post(SITE.apiMainUrl + 'api/Authentication/LoginUser', user)
        .then(response => {
            //console.log(response.data);
            if (response.data.result.status) {

                const authUser = response.data.result.data;
                //console.log(authUser);
                AuthHeaderToken(authUser.token);

                dispatch({
                    type: Types.SET_USER,
                    payload: {
                        user: authUser
                    }
                });
            }
            else {
                dispatch({
                    type: Types.USERS_ERROR,
                    payload: {
                        errors: response.data.result.errors,
                        status: response.data.result.status,
                        statusCode: response.data.result.statusCode,
                        timestamp: response.data.result.timestamp,
                    }
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
}

export const checkExistUser = (userName) => dispatch => {
    //console.log(userName);
    Axios.get(SITE.apiMainUrl + 'api/Authentication/v1/IsUserExist/' + userName)
        .then(response => {
            const { data,errors,message,status,statusCode,timestamp } = response.data;
            //console.log(response);
            dispatch({
                type: Types.CHECK_EXIST_USER,
                payload: {
                    existUser: {
                        isExist: status,
                        data,
                        errors,
                        message,
                        statusCode,
                        timestamp
                    }
                }
            });
        })
        .catch(error => {
            dispatchError(error, dispatch);
        })
}

export const signup = (user) => dispatch => {
    
    const uri = SITE.apiMainUrl + 'api/Authentication/v1/RegisterUser';
    //console.log('Action:',user,uri);
    Axios.post(uri, user)
        .then(response => {
            const { message,data,status, statusCode,errors,timestamp} = response.data;
            console.log('Axios response:',response);
            if (status) {                

                dispatch({
                    type: Types.SIGNUP,
                    payload: {
                        message,
                        status,
                        statusCode,
                        timestamp
                    }
                });
            }
            else {
                responseError(errors,status,statusCode,timestamp,dispatch);
            }
        })
        .catch(error => {
            dispatchError(error, dispatch);
        })
}

export const updateStudentProfile = (user) => dispatch => {
    Axios.post(SITE.apiMainUrl + 'api/Authentication/UpdateStudentProfile', user, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        //console.log(response);
        if (response.data) {
            dispatch({
                type: Types.UPDATE_STUDENT_PROFILE,
                payload: {
                    data: response.data
                }
            });
        }
    })
        .catch(error => {
            console.log(error);
        })
}
export const logout = () => {
    //localStorage.removeItem('auth_token');
    //history.push('/');
    return {
        type: Types.LOGOUT,
        payload: {
            user: {}
        }
    }
}

export const updateBalance = (user) => dispatch => {
    Axios.post(SITE.apiMainUrl + 'api/Transaction/UpdateBalance', user)
        .then(response => {
            if (response.data) {
                dispatch({
                    type: Types.UPDATE_BALANCE,
                    payload: {
                        isUpdate: response.data
                    }
                });
            }
        })
        .catch(error => {
            console.log(error);
        })
}

export const recoverAccount = (user) => dispatch => {
    Axios.post(SITE.apiMainUrl + 'api/Authentication/RecoverCustomerAccount', user)
        .then(response => {            
            const { data,errors,message,status,statusCode,timestamp } = response.data;
            
            if(status) {
                //console.log('Auth action:',response.data);
                dispatch({
                    type: Types.RECOVER_PASSWORD,
                    payload: {
                        message,
                        statusCode,
                        timestamp
                    }
                });
            }
            else {
                responseError(errors,status,statusCode,timestamp,dispatch);
            }            
        })
        .catch(error => {
            dispatchError(error, dispatch);
        })
}

