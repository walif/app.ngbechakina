/*==========================
*   AUTHENTICATION
 * ========================*/
export const SET_USER = 'SET_USER';
export const CHECK_EXIST_USER = 'CHECK_EXIST_USER';
export const SIGNUP = 'SIGNUP';
export const LOGOUT = 'LOGOUT';
export const USERS_ERROR = 'USERS_ERROR';
export const UPDATE_BALANCE = 'UPDATE_BALANCE';
export const UPDATE_STUDENT_PROFILE = 'UPDATE_STUDENT_PROFILE';
export const RECOVER_PASSWORD = 'RECOVER_PASSWORD';

/*==========================
*   MESSAGE
 * ========================*/
export const LOAD_MESSAGES = 'LOAD_MESSAGES';
export const LOAD_MESSAGE_BY_MESSAGE_ID = 'LOAD_MESSAGE_BY_MESSAGE_ID';

/*==========================
*   CATEGORY
 * ========================*/
export const REQUEST_CATEGORY = 'REQUEST_CATEGORY';
export const LOAD_CATEGORIES = 'LOAD_CATEGORIES';
export const LOAD_SUBCATEGORIES_BY_CATEGORY_ID = 'LOAD_SUBCATEGORIES_BY_CATEGORY_ID';
export const CATEGORY_RESPONSE_ERROR = 'CATEGORY_RESPONSE_ERROR';
export const HIDE_CATEGORY_MODAL = 'HIDE_CATEGORY_MODAL';

/*==========================
*   PRODUCT
 * ========================*/
export const REQUEST_PRODUCT = 'REQUEST_PRODUCT';
export const HIDE_PRODUCT_MODAL = 'HIDE_PRODUCT_MODAL';
export const PRODUCT_RESPONSE_ERROR = 'PRODUCT_RESPONSE_ERROR';
export const LOAD_PRODUCTS = 'LOAD_PRODUCTS';
export const LOAD_PRODUCTS_BY_CATEGORY_ID = 'LOAD_PRODUCTS_BY_CATEGORY_ID';
export const LOAD_PRODUCTS_BY_CATEGORY_ID_STATE = 'LOAD_PRODUCTS_BY_CATEGORY_ID_STATE';
export const LOAD_PRODUCTS_BY_CUSTOMER_ID = 'LOAD_PRODUCTS_BY_CUSTOMER_ID';
export const LOAD_PRODUCTS_BY_APPROVAL_ID = 'LOAD_PRODUCTS_BY_APPROVAL_ID';
export const LOAD_PRODUCT_BY_PRODUCT_ID = 'LOAD_PRODUCT_BY_PRODUCT_ID';
export const LOAD_PENDING_PRODUCTS = 'LOAD_PENDING_PRODUCTS';
export const SAVE_PRODUCT = 'SAVE_PRODUCT';
export const APPROVED_PRODUCT = 'APPROVED_PRODUCT';
export const CANCEL_PRODUCT = 'CANCEL_PRODUCT';

/*==========================
*   ADDRESS
 * ========================*/
export const LOAD_AREAS = 'LOAD_AREAS';