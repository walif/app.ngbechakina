﻿import * as Types from '../actions/types';

const initialState = {
    showModal: false,
    loaderImageType: 'loader',
    loaderMessage: 'Loading...',
    loaderLoop: true,
    categories: [],
    subCategories: [],
    data: {},
    errors: {},
    status: null,
    statusCode: null,
    timestamp: null
}

const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.REQUEST_CATEGORY: {
            return {
                ...state,
                showModal: true,
                loaderImageType: 'loader',
                loaderMessage: 'Loading...',
                loaderLoop: true,
                timestamp: Date.now()
            }
        }
        case Types.HIDE_CATEGORY_MODAL: {
            return {
                ...state,
                showModal: false,
                loaderImageType: 'loader',
                loaderMessage: 'Loading...',
            }
        }
        case Types.LOAD_CATEGORIES: {
            return {
                ...state,
                showModal:false,
                errors: {},
                categories: action.payload.data,
                status: action.payload.status,
                statusCode: action.payload.statusCode,
                timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_SUBCATEGORIES_BY_CATEGORY_ID: {
            return {
                ...state,
                errors: {},
                subCategories: action.payload.data,
                status: action.payload.status,
                statusCode: action.payload.statusCode,
                timestamp: action.payload.timestamp
            }
        }
        case Types.CATEGORY_RESPONSE_ERROR: {
            return {
                ...state,
                loaderMessage: action.payload.errors.toString(),
                loaderImageType: 'error',
                errors: action.payload.errors,
                // status: action.payload.status,
                // statusCode: action.payload.statusCode,
                // timestamp: action.payload.timestamp
            }
        }
        default: return state;
    }
}
export default categoryReducer