﻿import * as Types from '../actions/types';

const messageReducer = (state = [], action) => {
    switch (action.type) {        
        case Types.LOAD_MESSAGES: {
            return {
                ...state,
                messages: action.payload.messages                
            }
        }
        case Types.LOAD_MESSAGE_BY_MESSAGE_ID: {
            return {
                ...state,
                message: action.payload.message                
            }
        }
        default: return state;
    }
}
export default messageReducer