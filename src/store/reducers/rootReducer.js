import { combineReducers } from 'redux';
import { authReducer, messageReducer, categoryReducer, productReducer } from './';

const rootReducers = combineReducers({
    auth: authReducer,
    message: messageReducer,
    category: categoryReducer,
    product: productReducer,
})
export default rootReducers