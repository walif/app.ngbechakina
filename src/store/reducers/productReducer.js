﻿import * as Types from '../actions/types';

const initialState = {
    showModal: false,
    loaderImageType: 'loader',
    loaderMessage: 'Loading...',
    loaderLoop: true,
    products: [],
    product: null,
    areas: [],
    filterProducts: [],
    data: {},
    errors: {},
    success: {},
    status: null,
    statusCode: null,
    timestamp: null
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.REQUEST_PRODUCT: {
            return {
                ...state,
                showModal: true,
                loaderImageType: 'loader',
                loaderMessage: 'Loading...',
                loaderLoop: true,
                timestamp: Date.now()
            }
        }
        case Types.HIDE_PRODUCT_MODAL: {
            return {
                ...state,
                showModal: false,
                errors: {},
                success: {},
                loaderImageType: 'loader',
                loaderMessage: 'Loading...',
            }
        }
        case Types.LOAD_PRODUCTS: {
            return {
                ...state,
                errors: {},
                showModal: false,
                products: action.payload.data,
                // status: action.payload.status,
                // statusCode: action.payload.statusCode,
                // timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_PRODUCTS_BY_CATEGORY_ID: {
            //console.log('Reducer:',action.payload.data);
            return {
                ...state,
                errors: {},
                filterProducts: action.payload.data,
                // status: action.payload.status,
                // statusCode: action.payload.statusCode,
                // timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_PRODUCTS_BY_CUSTOMER_ID: {
            return {
                ...state,
                showModal: false,
                errors: {},
                products: action.payload.data,
                status: action.payload.status,
                statusCode: action.payload.statusCode,
                timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_PRODUCTS_BY_APPROVAL_ID: {
            return {
                ...state,
                showModal: false,
                errors: {},
                products: action.payload.data,
                // status: action.payload.status,
                // statusCode: action.payload.statusCode,
                // timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_PENDING_PRODUCTS: {
            return {
                ...state,
                showModal: false,
                errors: {},
                products: action.payload.data,
                status: action.payload.status,
                statusCode: action.payload.statusCode,
                timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_AREAS: {
            return {
                ...state,
                showModal: false,
                errors: {},
                areas: action.payload.data,
            }
        }
        case Types.APPROVED_PRODUCT: {
            let newProducts = [...state.products];
            newProducts = newProducts.filter(c => c.productId !== action.payload.data.productId);
            return {
                ...state,
                loaderImageType: 'success',
                loaderMessage: action.payload.message,
                errors: {},
                products: newProducts
            }
        }
        case Types.CANCEL_PRODUCT: {
            let newProducts = [...state.products];
            newProducts = newProducts.filter(c => c.productId !== action.payload.data.productId);
            return {
                ...state,
                loaderImageType: 'success',
                loaderMessage: action.payload.message,
                errors: {},
                products: newProducts
            }
        }
        case Types.LOAD_PRODUCTS_BY_CATEGORY_ID_STATE: {
            let errors = {};
            const { categoryId } = action.payload.data;
            const products = [...state.products];
            const filterProducts = products.filter(c => c.categoryId === categoryId);
            if (!filterProducts) {
                errors = Object.assign({}, "");
            }
            return {
                ...state,
                errors,
                filterProducts,
                status: action.payload.status,
                statusCode: action.payload.statusCode,
                timestamp: action.payload.timestamp
            }
        }
        case Types.SAVE_PRODUCT: {
            //console.log('Product reducer:',action.payload);
            return {
                ...state,
                errors: {},
                loaderImageType: 'success',
                loaderMessage: action.payload.message,
                success: {
                    status: action.payload.status,
                    statusCode: action.payload.statusCode
                },
                timestamp: action.payload.timestamp
            }
        }
        case Types.LOAD_PRODUCT_BY_PRODUCT_ID: {
            return {
                ...state,
                showModal: false,
                errors: {},
                product: action.payload.data,
                timestamp: action.payload.timestamp
            }
        }
        case Types.PRODUCT_RESPONSE_ERROR: {
            // let showModal = [...showModal];
            // if(showModal === false)
            //     showModal = true;
            //console.log(action.payload.errors.toString());
            return {
                ...state,
                showModal: true,
                loaderMessage: action.payload.errors.toString(),
                loaderImageType: 'error',
                errors: action.payload.errors,
                success: {},
                filterProducts: [],
                products: []
                // status: action.payload.status,
                // statusCode: action.payload.statusCode,
                // timestamp: action.payload.timestamp
            }
        }
        default: return state;
    }
}
export default productReducer