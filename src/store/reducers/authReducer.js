import * as Types from '../actions/types';
import AsyncStorage from '@react-native-async-storage/async-storage';
const init = {
    isLoading: true,
    isAuthenticated: false,
    user: null,
    error: {},
    success: {},
    existUser: {},
    recoveryPassword:{}
}
const authReucer = (state = init, action) => {
    switch (action.type) {
        case Types.SET_USER: {
            //console.log(action.payload.user);
            if (action.payload.user.expiresIn > Date.now()) {
                state.isAuthenticated = true;
                state.user = action.payload.user;
                AsyncStorage.setItem('token', JSON.stringify(state.user));
            }
            return {
                isLoading: false,
                user: state.user,
                isAuthenticated: state.isAuthenticated,
                error: {}
            }
        }
        case Types.USERS_ERROR: {
            return {
                ...state,
                error: {
                    message: action.payload.errors.toString(),
                    statusCode: action.payload.statusCode,
                    timestamp: action.payload.timestamp
                },
                success: {}
            }
        }
        case Types.CHECK_EXIST_USER: {
            return {
                ...state,
                existUser: action.payload.existUser,
                error: {},
                success: {}
            }
        }
        case Types.SIGNUP: {
            return {
                ...state,
                isLoading: false,
                success: {
                    message: action.payload.message,
                    status: action.payload.status,
                    statusCode: action.payload.statusCode,
                    timestamp: action.payload.timestamp
                },
                error: {}
            }

        }
        case Types.LOGOUT: {
            AsyncStorage.setItem('token', '');
            return {
                ...state,
                isLoading: false,
                isAuthenticated: false,
                user: null
            }
        }
        case Types.UPDATE_BALANCE: {
            return {
                ...state,
                isLoading: false,
                isUpdate: action.payload.isUpdate
            }
        }
        case Types.UPDATE_STUDENT_PROFILE: {

            let message = "";
            let result = "";
            let statusCode = "";
            if (action.payload.data.success) {
                result = JSON.stringify(action.payload.data.result);
                message = action.payload.data.success.message;
                statusCode = action.payload.data.success.statusCode;
                AsyncStorage.setItem('token', result);
            }
            else
                message = action.payload.data.error.message;

            return {
                ...state,
                isLoading: false,
                user: result,
                message: message,
                statusCode: statusCode
            }
        }
        case Types.RECOVER_PASSWORD: {
            //console.log(action);
            return {
                ...state,
                error: {},
                success: { 
                    message: action.payload.message,
                    statusCode: action.payload.statusCode,
                    timestamp: action.payload.timestamp,
                }
            }            
        }
        default: return state
    }
}
export default authReucer