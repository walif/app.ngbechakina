import { Text } from 'native-base';
import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
import { COLOR } from '../../../core';

const MainHeader = ({ title }) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.icon}
        onPress={() => navigation.goBack()}>
        <IconFA name="long-arrow-left" size={25} color="#fff" />
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={{ color: '#fff' }}>{title}</Text>
      </View>
    </View>
  );
};

export default MainHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: COLOR.BG_SECONDARY_COLOR,
    paddingVertical: 10
  },
  icon: {
    width: 50,
    marginHorizontal: 10,
    // borderWidth: 1,
    // borderColor: '#fff',
    alignItems: 'center',
    justifyContent:'center',
    paddingHorizontal: 5
  },
  title: { 
    flex: 1,
    // borderWidth: 1, 
    // borderColor: 'red', 
    // alignItems: 'center', 
    justifyContent: 'center' 
  }
})