import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Image, StyleSheet, Button, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { RVText } from '../../../core';
import { SearchOne } from '../Search';
import FocusAwareStatusBar from '../../../core/FocusAwareStatusBar';

class CustomHeader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isSubmitClick: false,
      balance: 0.00
    }
  }

  async componentDidMount() {
    this.updateBalance();

    this.updateBalanceListener = this.props.navigation.addListener('focus', () => {
      this.updateBalance();
    });
  }

  updateBalance = async () => {
    let balance = await AsyncStorage.getItem('balance');
    balance = parseFloat(balance).toFixed(2);
    this.setState({
      ...this.state,
      balance: balance
    });
  }

  submitHandler() {

  }


  render() {
    let { isHome, title, navigation, isSearch, searchHandler, placeholder, isRightAvatar } = this.props;
    return (
      <View style={styles.headerContainer}>
        <FocusAwareStatusBar backgroundColor='#83d7ffde' />
        {
          //LEFT SIDE BAR LAYOUT
        }
        <View style={{ width:30,justifyContent: "center", marginLeft: 10 }}>
          {
              isHome ?
                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                  <IconAntDesign name="menu-fold" size={20} color="#fff" />
                </TouchableOpacity>
                :
                <TouchableOpacity
                  style={{ flexDirection: 'row', alignItems: "center" }}
                  onPress={() => navigation.goBack()}
                >
                  <Ionicons name="chevron-back" size={25} color="#fff" />
                  <RVText content='Back' style={{ fontSize: 12, marginLeft: -6, color: '#fff' }} />
                </TouchableOpacity>
          }
        </View>
        {
          //CENTER SIDE BAR LAYOUT
        }
        <View style={{ flex: 1,justifyContent:'center',alignItems:'center',marginRight:30}}>          
          {
            title &&
            <View>
              <RVText content={title} style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }} />
            </View>
          }
          {
            isSearch &&
            <View style={{ paddingRight: 10 }}>
              <SearchOne searchHander={searchHandler} placeholder={placeholder} />
            </View>
          }
        </View>
        {
          //RIGHT SIDE BAR LAYOUT
          isRightAvatar &&
          <View style={{ flex: 1.2 }}>
            {
              <View style={{ flexDirection: "row", marginTop: 2 }}>
                <View style={styles.imageContainer}>
                  <Image source={require('../../../assets/images/user.png')} style={{ width: 40, height: 50, resizeMode: "cover" }} />
                </View>
                <View>
                  <View style={styles.balanceRoundShape}>
                    <RVText content={this.state.balance + ' ৳.'} style={{ fontSize: 13, fontWeight: 'bold', color: '#fff' }} />
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <View>
                      <View style={styles.liveCollege}></View>
                    </View>
                    <View>
                      <RVText content='Live' style={{ fontSize: 11, marginLeft: 2, fontWeight: 'bold', color: '#fff' }} />
                    </View>
                  </View>
                </View>
              </View>
            }
          </View>
        }

      </View>
    )
  }
}
const mapStateToProps = state => ({
  auth: state.auth,
  exam: state.exam
})
export default connect(mapStateToProps, null)(CustomHeader);

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    height: 60,
    marginTop: 0,
    backgroundColor: '#1c1c6d'
  },
  imageContainer: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
    marginTop: 0,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: "#d7d7d7",
    zIndex: 1000
  },
  balanceRoundShape: {
    borderWidth: 2,
    borderColor: '#d7d7d7',
    paddingLeft: 10,
    paddingRight: 8,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    marginTop: 8,
    marginLeft: -5,
    zIndex: -1000
  },
  liveCollege: {
    padding: 3,
    borderWidth: 1,
    borderColor: '#0d580d',
    borderRadius: 3,
    backgroundColor: '#008000',
    marginTop: 4
  },
  btnFinishExam: {
    paddingLeft: 8,
    paddingRight: 8,
    marginTop: 2,
    borderWidth: 1,
    borderColor: '#fff'
  },
  txtFinishExam: {
    fontSize: 11,
    color: '#fff',
    fontWeight: '700',
    // textTransform:'uppercase'
  }
})