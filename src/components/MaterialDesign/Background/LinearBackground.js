import React from 'react';
import { View,StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LinearBackground = ({ children }) => {
    //console.log(children);
    return (
        <LinearGradient colors={['#47b3fe', '#fff', '#fff']} style={styles.linearGradient}>
            {children}
        </LinearGradient>
    );
};

export default LinearBackground;

var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
      paddingLeft: 15,
      paddingRight: 15,
      borderRadius: 5
    }
  });