import AuthBackground from './AuthBackground';
import AuthBackgroundTwo from './AuthBackgroundTwo';
import WaveBackground from './WaveBackground';

export {
    AuthBackground,
    AuthBackgroundTwo,
    WaveBackground
}