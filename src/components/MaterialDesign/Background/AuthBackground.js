import React, { useState, useEffect } from 'react';
import {
    View, SafeAreaView, StyleSheet,
    Image, ImageBackground, StatusBar,
    KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard
} from 'react-native';
import { Content } from 'native-base';
import { IMAGE } from '../../../assets/image';

const AuthScreen = ({ children }) => {

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, []);

    const [footerStatus, setFooterStatus] = useState(true);
    const _keyboardDidShow = () => setFooterStatus(false);
    const _keyboardDidHide = () => setFooterStatus(true);

    // console.log(footerStatus);
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={{ flex: 1, backgroundColor: '#fff' }}>
                        <View style={{ flex: 1 }}>
                            <StatusBar backgroundColor='#22226d' />
                            <View style={styles.authTop}>
                                <ImageBackground source={IMAGE.AUTH_HEADER_BACKGROUND}
                                    style={styles.authImageContainer}
                                    resizeMode={"cover"} />
                            </View>
                            <View style={styles.authBody}>
                                <Content>
                                    {children}
                                </Content>
                            </View>

                            {
                                footerStatus &&
                                <View style={styles.authFooter}>
                                    <View style={{ flex: 1, flexDirection: "row", padding: 15 }}>
                                        <View style={{ flex: 1 }}>
                                            <Image source={IMAGE.APP_POWER_BY} style={styles.authPoweredBy} />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Image source={IMAGE.APP_DEVELOP_BY} style={styles.authImageDevelopedBy} />
                                        </View>
                                    </View>
                                </View>
                            }

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default AuthScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    authTop: {
        flex: 2,
        backgroundColor: '#fff'
    },
    authImageContainer: {
        alignSelf: 'center',
        height: '100%',
        width: '100%'
    },
    authPoweredBy: {
        resizeMode: "contain",
        width: 150,
        marginRight: 20
    },
    authImageDevelopedBy: {
        resizeMode: "contain",
        width: 120,
        alignSelf: "flex-end"
    },
    authBody: {
        flex: 2
        // borderWidth: 1,
        // borderColor:'red'
    },
    authFooter: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    }
})