import React, { useRef, useEffect } from 'react';
import {
  View, StyleSheet, Dimensions, SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback, Keyboard,
  ImageBackground, ScrollView,TouchableOpacity
} from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import FocusAwareStatusBar from '../../../core/FocusAwareStatusBar';
import WaveBackground from './WaveBackground';
import { IMAGE } from '../../../assets/image';

const AuthBackgroundTwo = ({ children, isLeftIcon,leftIconColor }) => {

  const navigation = useNavigation();
  //console.log(isLeftIcon);
  //   const scrollRef = useRef(null);

  //   useEffect(() => {
  //     Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
  //     Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

  //     // cleanup function
  //     return () => {
  //         Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
  //         Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
  //     };
  // }, []);

  // const _keyboardDidShow = () => {
  //   //console.log(scrollRef.current);
  //   scrollRef.current.scrollTo({ y: 5,x:5, animated: true });
  // }
  // const _keyboardDidHide = () => {};


  return (
    <>
      <FocusAwareStatusBar backgroundColor='#0099ff' />
      <WaveBackground
        customStyles={styles.svgCurve}
        customHeight={300}
        customTop={250}
        customBgColor="#0099ff"
        customWavePattern="M0,192L40,170.7C80,149,160,107,240,90.7C320,75,400,85,480,101.3C560,117,640,139,720,160C800,181,880,203,960,208C1040,213,1120,203,1200,165.3C1280,128,1360,64,1400,32L1440,0L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
      />

      <SafeAreaView style={{ flex: 1,position:'relative' }}>
        <>
          {
            isLeftIcon &&
            <TouchableOpacity
              onPress={() => { navigation.goBack() }}
              style={{ position: 'relative',top:10,zIndex:3,left:10, width:50,height:30, justifyContent:'center',alignItems:'center' }}
            >
              <IconFA name="long-arrow-left" size={20} color={leftIconColor} />
            </TouchableOpacity>
          }
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ width: 130, height: 130, borderColor: '#d5d5d5', borderWidth: 2, borderRadius: 130 / 2, marginTop: 20 }}>
                    <ImageBackground source={IMAGE.APP_LOGO} style={{ width: '100%', height: '100%' }} />
                  </View>
                </View>
              </View>
              <View style={styles.contentContainer}>
                {children}
              </View>
              <View style={{ flex: 1 }}></View>
            </View>
          </ScrollView>
        </>
        {/* </KeyboardAvoidingView> */}
      </SafeAreaView>
    </>
  )
}

export default AuthBackgroundTwo;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  // rest of the styles
  svgCurve: {
    position: 'absolute',
    width: Dimensions.get('window').width
  },
  authButton: {
    marginTop: 20,
    justifyContent: "center",
    color: '#21216d',
    flexWrap: 'wrap'
  },
  contentContainer: {
    flex: 3,
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: 30,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 40,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }
});