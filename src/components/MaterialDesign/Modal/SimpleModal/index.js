import SimpleModalController from './SimpleModalController';
import SimpleModalScreen from './SimpleModalScreen';

export {
    SimpleModalController,
    SimpleModalScreen
}