import React from 'react';
import { Modal, View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';

const SimpleModalScreen = ({ showModal, children, url, closeHandler, closeIcon }) => {

  //imageUri = require('../../assets/lottiefiles/loading.json');
  // const returnValue = {
  //   imageType,content,url
  // }
  //console.log('LMS:',returnValue);
  return (
    <Modal transparent visible={showModal}>
      <View style={styles.modalBackGround}>
        <View style={styles.modalContainer}>
          {
            closeIcon  && (
              <View style={{ alignItems: 'center' }}>
                <View style={styles.header}>
                  <TouchableOpacity
                    onPress={() => closeHandler()}
                  >
                    <IconMC name="close-box" size={35} color="#fff" style={{ backgroundColor: '#000' }} />
                    {/* <Ionicons name="close" size="30" /> */}
                    {/* <Image
                  source={require('../../../../assets/images/x.png')}
                  style={{ height: 15, width: 15,backgroundColor:'#fff' }}
                /> */}
                  </TouchableOpacity>
                </View>
              </View>
            )
          }

          {
            <View style={{ flexDirection: 'row', zIndex: 1, justifyContent: 'center', alignItems: 'center' }}>
              {children}
            </View>
          }
        </View>
      </View>
    </Modal>

  )
}

export default SimpleModalScreen;

const styles = StyleSheet.create({
  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    // height:'50%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
  },
  header: {
    width: '100%',
    // height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 3,
    top: 0
  },
})

