import React from 'react';
// import Animated from 'react-native-reanimated';
// import { Text } from 'react-native';
import SimpleModalScreen from './SimpleModalScreen';

const SimpleModalController = ({ visible, children, redirectTo, closeModal,closeIcon }) => {

    const [showModal, setShowModal] = React.useState(visible);
    // const [animationHeight,setAnimationHeight] = React.useState(200);   

    React.useEffect(() => {
        toggleModal();
    }, [visible]);

    const toggleModal = () => {
        if (visible) {
            setShowModal(true);
            // Animated.spring(scaleValue, {
            //     toValue: 1,
            //     duration: 300,
            //     useNativeDriver: true,
            // }).start();
        } else {
            setTimeout(() => setShowModal(false), 200);
            // Animated.timing(scaleValue, {
            //     toValue: 0,
            //     duration: 300,
            //     useNativeDriver: true,
            // }).start();
        }
    };
    return (
        <>
            <SimpleModalScreen
                showModal={showModal}                
                children={children}
                url={redirectTo}
                closeHandler={closeModal}
                closeIcon={closeIcon} />
        </>
    );
};

export default SimpleModalController