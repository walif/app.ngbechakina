import { LoaderModalController,LoaderModalScreen } from './loaderModal';
import { SimpleModalController,SimpleModalScreen } from './SimpleModal';

export {
    LoaderModalController,
    LoaderModalScreen,
    SimpleModalController,
    SimpleModalScreen
}