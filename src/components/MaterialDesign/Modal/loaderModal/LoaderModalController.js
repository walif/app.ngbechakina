import React from 'react';
// import Animated from 'react-native-reanimated';
import ServerErrorModalScreen from './ServerErrorModalScreen';
import ErrorModalScreen from './ErrorModalScreen';
import SuccessModalScreen from './SuccessModalScreen';
import LoadingModalScreen from './LoadingModalScreen';

const LoaderModalController = ({ visible, imageType, isLoop, content, redirectTo, closeModal }) => {

    const [showModal, setShowModal] = React.useState(visible);
    // const [imageUri, setImage] = React.useState('');
    // const [animationHeight,setAnimationHeight] = React.useState(200);   

    //console.log(visible);

    // React.useEffect(() => {
    //     //console.log(imageType);

    // }, [visible])


    React.useEffect(() => {
        // toggleModal();
        setShowModal(visible);
    }, [visible]);

    // React.useEffect(() => {
    //     console.log('Image Type:',imageType);
    //     //GET IMAGE LOADER
    //     if(imageHeight) 
    //         setAnimationHeight(imageHeight);

    //     //ANIMATION LOOPING
    //     GetImageUri();
    // },[imageType])

    // const GetImageUri = () => {
    //     imageType = imageType && imageType.toLowerCase();
    //     switch (imageType) {
    //         case 'server_error':
    //             setImage(require('../../../../assets/lottiefiles/fc-error-rays.json'));
    //             break;
    //         case 'error':
    //             setImage(require('../../../../assets/lottiefiles/error.json'));
    //             break;
    //         case 'success':
    //             setImage(require('../../../../assets/lottiefiles/success.json'));
    //             break;
    //         default:
    //             setImage(require('../../../../assets/lottiefiles/loading.json'));
    //     }
    // }


    imageType = imageType && imageType.toLowerCase();
    switch (imageType) {
        case 'server_error':
            return (<ServerErrorModalScreen
                showModal={showModal}
                imageType={imageType}
                content={content}
                url={redirectTo}
                isloop={isLoop}
                closeHandler={closeModal} />)
        case 'error':
            return (<ErrorModalScreen
                showModal={showModal}
                imageType={imageType}
                content={content}
                url={redirectTo}
                isloop={isLoop}
                closeHandler={closeModal} />)
        case 'success':
            return ( <SuccessModalScreen
                showModal={showModal}
                imageType={imageType}
                content={content}
                url={redirectTo}
                isloop={isLoop}
                closeHandler={closeModal} /> ) 
        default:
            return ( <LoadingModalScreen
                showModal={showModal}
                imageType={imageType}
                content={content}
                url={redirectTo}
                isloop={isLoop}
                closeHandler={closeModal} /> )
    }

    // const toggleModal = () => {
    //     if (visible) {
    //         setShowModal(true);
    //         // Animated.spring(scaleValue, {
    //         //     toValue: 1,
    //         //     duration: 300,
    //         //     useNativeDriver: true,
    //         // }).start();
    //     } else {
    //         setTimeout(() => setShowModal(false), 200);
    //         // Animated.timing(scaleValue, {
    //         //     toValue: 0,
    //         //     duration: 300,
    //         //     useNativeDriver: true,
    //         // }).start();
    //     }
    // };
    //console.log('Modal Controller:',content);
};

export default LoaderModalController