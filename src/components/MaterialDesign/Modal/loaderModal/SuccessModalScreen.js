import React from 'react';
import { Modal, View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';

const SuccessModalScreen = ({ showModal,imageType,content, url,isloop, closeHandler }) => {

  const returnValue = {
    imageType,content,url
  }
  return (
    <Modal transparent visible={showModal}>
      <View style={styles.modalBackGround}>
        <View style={styles.modalContainer}>
          <View style={{ alignItems: 'center' }}>
            <View style={styles.header}>
              <TouchableOpacity onPress={() => closeHandler(returnValue)}>
                <Image
                  source={require('../../../../assets/images/x.png')}
                  style={{ height: 20, width: 20 }}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ alignItems: 'center' }}>
              <LottieView source={require('../../../../assets/lottiefiles/success.json')} 
                autoPlay 
                loop={isloop} 
                style={{ height: 200 }} />
            </View>
            <Text selectable={true} style={{ fontSize: 16, textAlign: 'center', position: 'relative', bottom: 0 }}>{content}</Text>
        </View>
      </View>
    </Modal>

  )
}
const styles = StyleSheet.create({
  modalBackGround: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    width: '80%',
    // height:'50%',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 20,
    elevation: 20,
  },
  header: {
    width: '100%',
    // height: 40,
    alignItems: 'flex-end',
    justifyContent: 'center',
    position: 'absolute'
  },
})

export default SuccessModalScreen;