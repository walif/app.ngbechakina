import React from 'react';
import { SafeAreaView,View,StyleSheet, ColorPropType } from 'react-native';
import { COLOR } from '../../../core';

const MainLayout = ({children}) => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>{children}</View>
        </SafeAreaView>
    );
};

export default MainLayout;

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: COLOR.BG_PRIMARY_COLOR
    }
})