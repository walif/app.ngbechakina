import React,{ Component } from 'react';
import {  View,  Text,  TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { RVText } from '../../../core';

class CustomMessageBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
    }

    componentDidMount() {
        
    }

    render() {
      let {isHome,title,navigation} = this.props;
        return (
            <View style={styles.container}> 
                <RVText content={this.state.message} style={{fontSize:12,fontWeight:'400'}}/>
            </View>
        )
    }
}
const mapStateToProps = state => ({
    message: state.message
  });

export default connect(mapStateToProps, null)(CustomMessageBox);

const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        justifyContent:"center",
        height: 80,
        marginTop: 0,
        alignItems:'center',
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#c1c1c138',
    }
})