import React, { useState, useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import NetInfoSubscription from './NetInfoSubscriptionScreen';

const NetInfoSubscriptionController = () => {
    //const APP_NAME = 'Example App';
    const [isConnected, setIsConnected] = useState(false);
    const [mounted, setMounted] = useState(false);

    //const fadeAnimation = useState(new Animated.Value(0))[0];

    useEffect(() => {
        //Intial status
        NetInfo.fetch().then((state) => {  
            //console.log('Net1:',state);         
            setIsConnected(state.isConnected);
            if (state.isConnected == false) {
                setMounted(true);
            }
        });
        //Internet connection listener
        NetInfo.addEventListener((state) => {  
            //console.log('Net2:',state);          
            setIsConnected(state.isConnected);
            if (state.isConnected == false) {
                setMounted(true);
            }
        });
    }, []);

    // const fadeIn = () => {
    //     Animated.timing(fadeAnimation, {
    //         toValue: 1,
    //         duration: 1000
    //     }).start();
    // };

    // const fadeOut = () => {
    //     Animated.timing(fadeAnimation, {
    //         toValue: 0,
    //         duration: 2000
    //     }).start();
    // };

    return <NetInfoSubscription
        isConnected={isConnected}
        mounted={mounted}/>
};

export default NetInfoSubscriptionController;