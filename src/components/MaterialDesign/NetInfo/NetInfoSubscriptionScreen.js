import React from 'react';
import { View,Text } from 'react-native';
import * as Animatable from 'react-native-animatable';

const NetInfoSubscriptionScreen = ({isConnected,mounted}) => {
    return (
        <React.Fragment>
          {!isConnected && (
            <Animatable.View
              style={{
                backgroundColor: 'rgba(255,0,0,0.6)',
                flexDirection: 'row',
                position: 'absolute',
                zIndex: 2,
                top: 0,
                width: '100%',
                height: 30,
                alignItems: 'center',
                alignContent: 'center',
                alignSelf: 'center'
              }} 
              animation="fadeInDown">
              <View style={{flex: 2}}>
                <Text
                  style={{
                    color: '#fff',
                    textAlign: 'center',
                    alignSelf: 'center',
                    fontWeight: '700',
                    fontSize:12
                  }}>
                  Please Check your Internet Connection.
                </Text>
              </View>
            </Animatable.View>
          )}
          {isConnected && mounted && (
            <Animatable.View
              style={{
                backgroundColor: 'rgba(0,128,0,0.6)',
                //borderTopLeftRadius: 40,
                flexDirection: 'row',
                position: 'absolute',
                zIndex: 2,
                top: 0,
                //width: Dimensions.get('window').width / 1.5,
                width:'100%',
                height: 30,
                alignItems: 'center',
                alignContent: 'center',
                alignSelf: 'center',
                //borderRadius: 50,
              }}  
              animation="fadeOutUp"
              duration={5000}
              delay={2000}>
              <View style={{flex: 2}}>
                <Text
                  style={{
                    color: '#fff',
                    textAlign: 'center',
                    alignSelf: 'center',
                    fontWeight: '700',
                    fontSize:12
                  }}>
                  You're back online!
                </Text>
              </View>
            </Animatable.View>
          )}
        </React.Fragment>
      );
    }

export default NetInfoSubscriptionScreen;