import React from 'react';
import { TextInput, StyleSheet,KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard } from "react-native";
const SearchOne = ({ placeholder,searchHander }) => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <TextInput
                    inlineImageLeft='search_icon'
                    style={styles.searchStyle}
                    placeholder={placeholder}
                    placeholderTextColor="#fff"
                    selectionColor="#fff"
                    textAlign="center"
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                    onChangeText={text => searchHander(text)}
                />
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>

    );
};
const styles = StyleSheet.create({
    searchStyle: {
        borderColor: '#fff',
        color: '#fff',
        padding: 0,
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 1,
        borderRadius: 25,
    }
});
export default SearchOne;