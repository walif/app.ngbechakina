import React,{ Component } from 'react';
import {  View,  Text,SafeAreaView,TouchableOpacity } from 'react-native';
import { CustomHeader } from '../index';
import { RVText } from '../core';

export class SettingScreen extends Component {
    render() {
      let {navigation} = this.props;
        return (
            <SafeAreaView style={{ flex: 1 }}>
              <CustomHeader title='Setting' isHome={true} navigation = {navigation}/>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <RVText content='Setting Screen!' />
                <TouchableOpacity 
                style={{marginTop:20}}
                onPress={() => navigation.navigate('SettingDetail')}>
                  <RVText content='Go to Setting Detail' />
                </TouchableOpacity>
              </View>      
            </SafeAreaView>
          );
    }
}