import { Input, Item, Label } from 'native-base';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import CheckBox from '@react-native-community/checkbox';
import { RVText } from '../../../../core';

const ProductContactScreen = ({
    product,
    areas,
    customerContactHandler,
    visibleCustomerContactHanlder,
    areaChangeHandler
}) => {
    //console.log('Areas:', areas);
    //console.log('Customer:', product.customer.areaName);
    return (
        <View style={styles.container}>
            <RVText content="নাম" style={{ fontSize: 14 }} />
            <RVText content={product.customer.userName} style={{ fontSize: 12 }} />
            <View style={styles.contact}>
                <View style={styles.inputView,{borderBottomColor:'#ccc',borderBottomWidth:1,marginBottom:10}}>
                <Label style={{ fontSize: 14,color:'#a0a0a0' }}>আপনার অবস্থান নির্ধারণ করুন</Label>
                    <Picker
                        note
                        mode="dropdown"
                        value={product.customer.areaName}
                        selectedValue={product.customer.areaName}
                        style={styles.picker}
                        onValueChange={(itemValue, itemPosition) => areaChangeHandler(itemValue, itemPosition)}
                    >
                        {
                            areas &&
                            areas.map((data, index) => {
                                return <Picker.Item key={index} label={data.areaName} value={data.areaId} />
                            })
                        }

                    </Picker>
                </View>
                <View style={styles.inputView}>
                    <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>ফোন নম্বর লিখুন</Label>
                        <Input
                            onChangeText={customerContactHandler}
                            value={product.customer.contactNo}
                            style={{ fontSize: 12 }}
                            keyboardType="number-pad" />
                    </Item>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 10, alignItems: 'center' }}>
                    <CheckBox
                        // disabled={false}
                        value={product.customer.isVisibleCustomerContact}
                        onValueChange={visibleCustomerContactHanlder}
                    />
                    <RVText content="আমার ফোন নম্বর গোপন রাখুন" style={{ fontSize: 12 }} />
                </View>
            </View>
            <RVText content="ইমেল" style={{ fontSize: 14 }} />
            <RVText content={product.customer.email} style={{ fontSize: 12 }} />
        </View>
    );
};

export default ProductContactScreen;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        paddingTop: 10
    },
    contact: {
        borderWidth: 1,
        borderColor: '#ccc',
        padding: 10,
        marginVertical: 10
    },
    inputView: {
        marginTop: 5,
        marginBottom: 10
    },
    picker: {
        width: '100%',
        height: 50
    }
})