import React from 'react';
import { useSelector } from 'react-redux';
import { MyAdsScreen } from '.';

const MyAdsController = () => {
    
    const authState = useSelector(state => state.auth);
    // console.log(authState);
    return (
        <>
            <MyAdsScreen user={authState.user}/>
        </>
    );
};

export default MyAdsController;