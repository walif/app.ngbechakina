import React, { useEffect } from 'react';
import { LoaderModalController } from '../../../MaterialDesign';
import ProductListScreen from './ProductListScreen';
import { getProductsByUserId, closeProductModel } from '../../../../store/actions/productAction';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

const ApprovedProductListController = () => {

    const navigation = useNavigation();

    /*=====================
    REDUX STATE
    ======================*/

    const authState = useSelector(state => state.auth);
    const { showModal, loaderImageType, loaderMessage, loaderLoop, products } = useSelector(state => state.product);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProductsByUserId());
    }, [])

    const closeModal = (returnValue) => {
        dispatch(closeProductModel());
    }

    const productDetailHandler = (product) => {
        // console.log(product);
        navigation.navigate('ProductDetails', { product });
    }
    //console.log('Show Modal:',showModal);

    return (
        <>
            <LoaderModalController
                visible={showModal}
                imageType={loaderImageType}
                content={loaderMessage}
                isLoop={loaderLoop}
                closeModal={closeModal} />

            <ProductListScreen
                title="অনুমোদিত বিজ্ঞাপন সমূহ"
                products={products}
                user={authState.user}
                productDetailHandler={productDetailHandler} />
        </>
    );
};

export default ApprovedProductListController;