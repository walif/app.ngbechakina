import { Button } from 'native-base';
import React from 'react';
import { View } from 'react-native';
import { RVText } from '../../../../core';
import { SimpleModalController } from '../../../MaterialDesign';

const ConfirmProductModalScreen = ({ confirmModal,serviceCharge, closeConfirmModal,saveProductHandler }) => {
    return (
        <SimpleModalController
            visible={confirmModal}
        >
            <View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', paddingBottom: 15 }}>
                    <RVText
                        content={`এই বিজ্ঞাপনটি প্রকাশ করতে ${serviceCharge} টাকা 01674562901 নম্বরে বিকাশ করুন।রাজি থাকলে সম্মতিতে ক্লিক করুন অন্যথায় বাতিল করুন।`}
                        style={{ fontSize: 12 }}
                    />
                </View>
                <View style={{ flexDirection: 'row',marginTop:10 }}>
                    <View style={{ flex: 1}}>
                        <Button small block danger style={{ marginBottom: 10 }} onPress={() => closeConfirmModal()}>
                            <RVText content='বাতিল' style={{ fontSize: 14, color: '#fff' }} />
                        </Button>
                    </View>
                    <View style={{ flex: 1}}></View>
                    <View style={{ flex: 1}}>
                        <Button small block success onPress={() => saveProductHandler()}>
                            <RVText content='সম্মতি' style={{ fontSize: 14, color: '#fff' }} />
                        </Button>
                    </View>
                </View>
            </View>
        </SimpleModalController>
    );
};

export default ConfirmProductModalScreen;