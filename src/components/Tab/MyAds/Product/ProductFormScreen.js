import React from 'react';
import { SafeAreaView, ScrollView, View, Text } from 'react-native';
import { List, Button } from 'native-base';
// import { useNavigation } from '@react-navigation/native';
import CheckBox from '@react-native-community/checkbox';
import { COLOR, RVText } from '../../../../core';
import { UploadImageScreen } from '../UploadImage';
import ProductInfoScreen from './ProductInfoScreen';
import { ProductContactScreen } from '../Contact';
import { MainHeader } from '../../../MaterialDesign';
import { _CategoryView } from '../Category';



const TextColor = ({ children }) => <Text
    style={{ color: COLOR.ICON_PRIMARY_COLOR, fontWeight: '700' }}
>{children}</Text>

const ProductFormScreen = ({
    categories,
    parentCat,
    visibleProductInfo,
    navigateCategory,
    navigateParentCategory,
    product,
    areas,
    imagesUrl,
    uploadHandler,
    deleteUploadImageHandler,
    productTitleHandler,
    productDescriptionHandler,
    productPriceHandler,
    priceAsDiscussionHandler,
    customerContactHandler,
    visibleCustomerContactHanlder,
    areaChangeHandler,
    termsHanlder,
    submitHandler
}) => {
    // console.log('Parent Cat:', parentCat);
    // console.log('Cat:', categories);
    //console.log('product info', product);
    //console.log('Areas:', areas);
    // const navigation = useNavigation();

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#efeaeacc' }}>
            <MainHeader title={'আপনার বিজ্ঞাপন পোস্ট করুন'} />
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                {
                    parentCat.length > 0 &&
                    <_CategoryView
                        categories={parentCat}
                        navigateCategory={navigateParentCategory} />
                }
                <List>
                    {
                        categories.length > 0 &&
                        <_CategoryView
                            categories={categories}
                            navigateCategory={navigateCategory} />
                    }
                </List>
                {
                    visibleProductInfo && (
                        <>
                            <UploadImageScreen
                                imagesUrl={imagesUrl}
                                uploadHandler={uploadHandler}
                                deleteUploadImageHandler={deleteUploadImageHandler}
                            />
                            <ProductInfoScreen
                                product={product}
                                productTitleHandler={productTitleHandler}
                                productDescriptionHandler={productDescriptionHandler}
                                productPriceHandler={productPriceHandler}
                                priceAsDiscussionHandler={priceAsDiscussionHandler}
                            />
                            <ProductContactScreen
                                product={product}
                                areas={areas}
                                customerContactHandler={customerContactHandler}
                                visibleCustomerContactHanlder={visibleCustomerContactHanlder}
                                areaChangeHandler={areaChangeHandler}
                            />
                            <View style={{ marginVertical: 10, marginHorizontal: 10 }}>
                                <View style={{ flexDirection: 'row', marginRight: 10, alignItems: 'center' }}>
                                    <CheckBox
                                        // disabled={false}
                                        value={product.terms}
                                        onValueChange={termsHanlder}
                                    />
                                    <Text style={{ fontSize: 14 }}>আমি <TextColor>শর্তাবলী ও নীতিমালাগুলো</TextColor> পড়েছি এবং গ্রহণ করেছি</Text>
                                </View>

                                <Button block
                                    style={[{
                                        backgroundColor: '#00aaff',
                                        marginHorizontal: 10,
                                        marginVertical: 20
                                    }]}
                                    onPress={submitHandler}>
                                    <RVText content={'বিজ্ঞাপন দিন'} style={{ fontSize: 16, color: '#fff' }} />
                                </Button>
                            </View>
                        </>
                    )
                }

            </ScrollView>
        </SafeAreaView>
    );
};

export default ProductFormScreen;