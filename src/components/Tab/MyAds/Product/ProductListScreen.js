import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { Body, Button, Left, List, ListItem, Right, Thumbnail, View } from 'native-base';
import IconFA5 from 'react-native-vector-icons/FontAwesome5'
import { MainHeader } from '../../../MaterialDesign';
import { COLOR, RVText } from '../../../../core';
import { IMAGE } from '../../../../assets/image';

const LoadProductContent = ({ product, user, cancelProductHandler, approvedProductHandler }) => (
    user && user.userType === 'admin' && product.actionStatus === 1 ? (
        <View style={styles.productContainer}>
            <TouchableOpacity
                onPress={() => cancelProductHandler(product)}
                style={styles.iconContent}>
                <IconFA5 name="trash-restore-alt" size={20} color={COLOR.COLOR_RED} />
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => approvedProductHandler(product)}
                style={[styles.iconContent, { borderLeftWidth: 1, borderLeftColor: '#ccc' }]}>
                <IconFA5 name="check" size={18} color={COLOR.COLOR_GREEN} />
            </TouchableOpacity>
        </View>
    ) : (
        <RVText content={product.priceAsDiscussion === false ? '৳. ' + product.productPrice : 'আলোচনা সাপেক্ষে'} style={{ fontSize: 12, fontWeight: 'bold' }} />
    )
)

const ProductListScreen = ({ title, products, user, cancelProductHandler, approvedProductHandler,productDetailHandler }) => {
    //console.log(products);
    return (
        <SafeAreaView style={{ flex: 1,backgroundColor:'#fff' }}>
            <MainHeader title={title} />
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                <List>
                    {
                        products && products.map((product, index) => {
                            let productImage = JSON.parse(product.productImages);
                            //console.log(product.priceAsDiscussion);
                            return (

                                <ListItem key={index} thumbnail onPress={ ()=> productDetailHandler(product)}>
                                    <Left>
                                        <Thumbnail square
                                            source={productImage[0] === '' || productImage[0] === null ? IMAGE.DEFAULT_PRODUCT : { uri: productImage[0] }}
                                            style={{ width: 50, height: 50 }} />
                                    </Left>
                                    <Body>
                                        <RVText content={product.productTitle} style={{ fontSize: 14 }} />
                                        <RVText content={`ক্যাটাগরিঃ ${product.categoryName}`} style={{ fontSize: 12 }} />
                                        <RVText content={`স্ট্যাটাসঃ ${product.status}`} style={{ fontSize: 12, fontWeight: 'bold' }} />
                                    </Body>
                                    <Right>
                                        <LoadProductContent
                                            product={product}
                                            user={user}
                                            cancelProductHandler={cancelProductHandler}
                                            approvedProductHandler={approvedProductHandler} />
                                    </Right>
                                </ListItem>

                            )
                        })
                    }
                </List>

            </ScrollView>
        </SafeAreaView>
    );
};

export default ProductListScreen;

const styles = StyleSheet.create({
    productContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderColor: '#ccc',
    },
    iconContent: {
        paddingHorizontal: 8,
        paddingVertical: 5,
    }
})