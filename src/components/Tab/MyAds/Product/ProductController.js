import React, { useEffect, useState } from 'react';
import { Toast } from 'native-base';
import { useDispatch, useSelector } from 'react-redux';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { LoaderModalController } from '../../../MaterialDesign';
import ProductFormScreen from './ProductFormScreen';
import { loadCategories, closeCategoryModel } from '../../../../store/actions/categoryAction';
import { saveProduct, closeProductModel, getAreas } from '../../../../store/actions/productAction';
import ConfirmProductModalScreen from './ConfirmProductModalScreen';

const productInit = {
    categoryId: 0,
    productTitle: '',
    productDescription: '',
    productPrice: '',
    priceAsDiscussion: false,
    productImages: [],
    terms: false,
    customer: {
        userName: '',
        contactNo: '',
        email: '',
        areaName: '--নির্বাচন করুন--',
        areaId: 0,
        isVisibleCustomerContact: false
    }
};

const initLoader = {
    showModal: false,
    imageType: 'loader',
    content: 'Loading...',
    isLoop: true,
    loaderType: ''
}

const initConfirmModal = { 
    showModal: false, 
    serviceCharge: 0 
}

const cameraOptions = {
    mediaType: 'photo',
    quality: 1,
    maxWidth: 1280,
    maxHeight: 720,
    selectionLimit: 5,
    includeBase64: true
}


const ProductController = () => {

    const [loader, setLoader] = useState(initLoader);
    const [categoryList, setCategoryList] = useState([]);
    const [parentCat, setParentCat] = useState([]);
    const [visibleProductInfo, setProductInfo] = useState(false);
    const [product, setProduct] = useState(productInit);
    const [imagesUrl, setImagesUrl] = useState([]);
    const [confirmModal, setConfirmModal] = useState(initConfirmModal);

    /*=====================
    REDUX STATE
    ======================*/
    const categoryState = useSelector(state => state.category);
    const authState = useSelector(state => state.auth);
    const productState = useSelector(state => state.product);
    const dispatch = useDispatch();

    //console.log(authState);

    useEffect(() => {
        setParentCat([]);
        dispatch(loadCategories());
        dispatch(getAreas());

        const { name, mobile, email } = authState.user;
        setProduct({
            ...product,
            customer: {
                ...product.customer,
                userName: name,
                contactNo: mobile,
                email
            }
        });
    }, []);


    useEffect(() => {
        //console.log('Cat State:',categoryState);
        setLoader({
            ...loader,
            showModal: categoryState.showModal,
            imageType: categoryState.loaderImageType,
            content: categoryState.loaderMessage,
            isLoop: categoryState.loaderLoop,
            loaderType: 'category'
        });
    }, [categoryState.showModal, categoryState.timestamp])


    useEffect(() => {
        setCategoryList(categoryState.categories);
    }, [categoryState.categories, categoryState.timestamp]);


    const navigateCategory = (category) => {
        setCategoryList(category.hasChildCategory);
        setParentCat([...parentCat, category]);
        // console.log(category);
        if (category.hasChildCategory.length === 0) {
            setProductInfo(true);
        }
    }

    const navigateParentCategory = (category) => {
        //console.log(parentCat);
        if (category.parentCategoryId === 0) {
            setParentCat([]);
            setCategoryList(categoryState.categories);
        }
        else {
            let child = parentCat.find(c => c.categoryId === category.parentCategoryId);
            let parent = parentCat.filter(c => c.categoryId !== category.categoryId);
            // console.log(child);
            // console.log(parent);
            setParentCat(parent);
            setCategoryList(child.hasChildCategory);
        }
        setProductInfo(false);
        //if(parentCat.parentCat)
    }

    const closeModal = (returnValue) => {
        if (loader.loaderType === 'category')
            dispatch(closeCategoryModel());
        else
            dispatch(closeProductModel());

        //console.log(returnValue);
        // this.setState({
        //   ...this.state,
        //   showModal: false
        // });
    }

    const closeConfirmModal = () => {
        setConfirmModal(initConfirmModal);
    }

    /*======================
    =======================*/

    const uploadHandler = () => {

        launchImageLibrary(cameraOptions, (fileObj) => {
            //console.log(fileObj);
            if (fileObj.assets) {

                let noOfImages = imagesUrl.length + fileObj.assets.length;
                if (noOfImages > 5) {
                    Toast.show({
                        text: "সর্বোচ্চ ৫টি ছবি যুক্ত করতে পারবেন।",
                        buttonText: "Ok",
                        position: "bottom",
                        textStyle: { fontSize: 14 },
                        buttonTextStyle: { fontSize: 12 },
                        style: {
                            backgroundColor: "red"
                        },
                        duration: 3000
                    });
                    return;
                }
                let images = [...imagesUrl];
                fileObj.assets.forEach(element => {
                    images = images.concat(element);
                    setImagesUrl(images);
                });
            }
        });
    }

    const deleteUploadImageHandler = (img) => {

        let images = [...imagesUrl];
        images = images.filter(c => c.uri !== img.uri);
        setImagesUrl(images);
        // console.log(img);
    }

    const productTitleHandler = (value) => {
        setProduct({
            ...product,
            productTitle: value
        });
        //console.log(product);
    }

    const productDescriptionHandler = (value) => {
        setProduct({
            ...product,
            productDescription: value
        });
        //console.log(product);
    }

    const productPriceHandler = (value) => {
        setProduct({
            ...product,
            productPrice: value
        });
        //console.log(product);
    }

    const priceAsDiscussionHandler = (value) => {
        // console.log(value);
        setProduct({
            ...product,
            productPrice: "",
            priceAsDiscussion: value
        });
        // console.log(product);
    }

    const termsHanlder = (value) => {
        // console.log(value);
        setProduct({
            ...product,
            terms: value
        });
        // console.log(product);
    }


    const customerContactHandler = (value) => {
        setProduct({
            ...product,
            customer: {
                ...product.customer,
                contactNo: value
            }
        });
    }

    const areaChangeHandler = (value, key) => {
        setProduct({
            ...product,
            customer: {
                ...product.customer,
                areaId: value
            }
        });
    }

    const visibleCustomerContactHanlder = (value) => {
        setProduct({
            ...product,
            customer: {
                ...product.customer,
                isVisibleCustomerContact: value
            }
        });
    }

    const validateData = () => {
        //console.log(product);
        const { customer, priceAsDiscussion, productDescription, productPrice, productTitle, terms } = product;
        let error = false;
        let message = "";

        if (parentCat.length === 0) {
            error = true;
            message = 'প্রডাক্টের ক্যাটাগরি নির্বাচন করুন।';
        }
        else if (imagesUrl.length === 0) {
            error = true;
            message = 'প্রডাক্টের ছবি যুক্ত করুন।';
        }
        else if (productTitle === "") {
            error = true;
            message = 'প্রডাক্টের শিরোনাম লিখুন।';
        }
        else if (productDescription === "") {
            error = true;
            message = 'প্রডাক্টের বর্ণনা লিখুন।';
        }
        else if (productPrice === "" && priceAsDiscussion === false) {
            error = true;
            message = 'প্রডাক্টের দাম নির্ধারণ করুন।';
        }
        else if (customer.contactNo === "") {
            error = true;
            message = 'আপনার মোবাইল নম্বর লিখুন।';
        }
        else if (customer.areaId === 0) {
            error = true;
            message = 'আপনার অবস্থান নির্ধারণ করুন।';
        }
        else if (terms === false) {
            error = true;
            message = 'শর্তাবলী চেক দিন।';
        }

        if (error) {
            Toast.show({
                text: message,
                buttonText: "Ok",
                position: "bottom",
                textStyle: { fontSize: 14 },
                buttonTextStyle: { fontSize: 12 },
                style: {
                    backgroundColor: "red"
                },
                duration: 3000
            });
            return error;
        }
        return error;
    }

    const submitHandler = () => {
        // console.log(parentCat.slice(-1).pop());
        // return;
        if (validateData()) {
            return;
        }

        const { serviceCharge } = parentCat.slice(-1).pop();

        if (serviceCharge > 0) {
            setConfirmModal({ showModal: true, serviceCharge});
        } else {
            saveProductHandler();
        }
    }

    const saveProductHandler = () => {

        // let images = productImages.join(',');
        //console.log(product);
        //console.log(imagesUrl);
        // console.log("Join:",images);

        // const myArr = images.split(",");
        // console.log('Split:',myArr);
        
        const { categoryId } = parentCat.slice(-1).pop();

        const { customer, priceAsDiscussion, productDescription, productPrice, productTitle } = product;
        let objectProduct = {
            categoryId,
            priceAsDiscussion,
            productDescription,
            productTitle
        };
        objectProduct.isVisibleCustomerContact = customer.isVisibleCustomerContact;
        objectProduct.areaId = customer.areaId;
        objectProduct.contactNo = JSON.stringify(customer.contactNo);
        objectProduct.productImages = JSON.stringify(imagesUrl);
        objectProduct.productPrice = priceAsDiscussion === false ? parseFloat(productPrice) : 0;
        //console.log(objectProduct);
        setConfirmModal(initConfirmModal);
        dispatch(saveProduct(objectProduct));
    }

    useEffect(() => {
        //console.log(productState);
        setLoader({
            ...loader,
            showModal: productState.showModal,
            imageType: productState.loaderImageType,
            content: productState.loaderMessage,
            isLoop: productState.loaderLoop,
            loaderType: 'product'
        });

    }, [productState.showModal, productState.timestamp]);


    useEffect(() => {
        if (Object.keys(productState.success).length !== 0) {
            //setProduct(productInit);
            setImagesUrl([]);
            const { name, mobile, email } = authState.user;
            setProduct({
                ...productInit,
                customer: {
                    ...productInit.customer,
                    userName: name,
                    contactNo: mobile,
                    email
                }
            });
        }
    }, [productState.success, productState.timestamp])


    //console.log(confirmModal);

    const { showModal, imageType, content, isLoop } = loader;

    return (
        <>

            <LoaderModalController
                visible={showModal}
                imageType={imageType}
                content={content}
                isLoop={isLoop}
                closeModal={closeModal} />

            <ConfirmProductModalScreen
                confirmModal={confirmModal.showModal}
                closeConfirmModal={closeConfirmModal}
                serviceCharge={ confirmModal.serviceCharge}
                saveProductHandler={saveProductHandler} />

            <ProductFormScreen
                categories={categoryList}
                parentCat={parentCat}
                visibleProductInfo={visibleProductInfo}
                navigateCategory={navigateCategory}
                navigateParentCategory={navigateParentCategory}
                product={product}
                areas={productState.areas}
                imagesUrl={imagesUrl}
                uploadHandler={uploadHandler}
                deleteUploadImageHandler={deleteUploadImageHandler}
                productTitleHandler={productTitleHandler}
                productDescriptionHandler={productDescriptionHandler}
                productPriceHandler={productPriceHandler}
                priceAsDiscussionHandler={priceAsDiscussionHandler}
                customerContactHandler={customerContactHandler}
                visibleCustomerContactHanlder={visibleCustomerContactHanlder}
                areaChangeHandler={areaChangeHandler}
                termsHanlder={termsHanlder}
                submitHandler={submitHandler} />
        </>
    );
};

export default ProductController;