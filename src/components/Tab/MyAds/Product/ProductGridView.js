import React from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { IMAGE } from '../../../../assets/image';
import { COLOR } from '../../../../core';
import moment from 'moment';
// import { useNavigation } from '@react-navigation/native';

// const navigation = useNavigation();

const ProductGridView = ({ pageName,itemWidth, product,productDetailHandler }) => {

    const productImage = JSON.parse(product.productImages);

    const timestamp = moment(new Date(product.approvedDateTime), 'ddd MMM DD YYYY HH:mm:ss GMT Z').fromNow(true);
    // const timestamp = moment(new Date(product.approvedDateTime), 'ddd MMM DD YYYY HH:mm:ss GMT Z').diff(Date.now(), 'hours');
    // console.log('Prod:',new Date(product.approvedDateTime));
    //console.log('product:', product);
    //console.log('width:', itemWidth);

    return (
        <TouchableOpacity
            style={[styles.container,pageName === 'product' ? styles.containerExtraStyle : null, { width: itemWidth }]}
            onPress={() => productDetailHandler(product)}>
            <Image
                source={productImage[0] === '' || productImage[0] === null ? IMAGE.DEFAULT_PRODUCT : { uri: productImage[0] }}
                style={styles.img} />
            <Text style={{ fontSize: 12, paddingHorizontal: 5, paddingVertical: 5 }}>{product.productTitle.length >= 45 ? product.productTitle.substr(0, 40) + '...' : product.productTitle}</Text>

            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 5 }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 11, fontWeight: 'bold' }}>{product.priceAsDiscussion === false ? '৳. ' + product.productPrice : 'আলোচনা সাপেক্ষে'}</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 11, fontWeight: 'bold', alignSelf: 'flex-end' }}>{timestamp + ' ago'}</Text>
                </View>
            </View>

        </TouchableOpacity>
    )
}

export default ProductGridView;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: 195
        // borderWidth:1,
        // borderColor:'red'
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    containerExtraStyle: {
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        margin: 10
    },
    img: {
        width: 150,
        height: 130,
        // borderWidth: 1,
        alignSelf: 'center',
        // borderColor: COLOR.COLOR_GREEN,
        resizeMode: 'contain'
    }
})