import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ProductDetailsScreen from './ProductDetailsScreen';
import { getProductByProductId } from '../../../../store/actions/productAction';


const ProductDetailsController = ({ route }) => {

    // const [product, setProduct] = useState(null);
    const [images, setImages] = useState([]);

    /*=====================
    REDUX STATE
    ======================*/
    // const categoryState = useSelector(state => state.category);
    // const authState = useSelector(state => state.auth);
    const { product } = useSelector(state => state.product);
    const dispatch = useDispatch();

    useEffect(() => {
        // console.log(route);
        // console.log(route.params.product.productId);
        dispatch(getProductByProductId(route.params.product.productId));
        //setProduct(route.params.product);
    }, [route])

    useEffect(() => {
        //console.log(product);
        if (product) {
            //let imgArray = [];
            const productImages = JSON.parse(product.productImages);
            // productImages.forEach((element) => {
            //     //console.log('EL:', element);
            //     imgArray = [...imgArray, {
            //         image: element,
            //         desc: 'Silent Waters in the mountains in midst of Himilayas',
            //     }]
            // });
            setImages(productImages);
        }
    }, [product])

    //console.log(productState);

    return (
        <>
            {
                product &&
                <ProductDetailsScreen
                    product={product}
                    images={images} />
            }
        </>
    );
};

export default ProductDetailsController;