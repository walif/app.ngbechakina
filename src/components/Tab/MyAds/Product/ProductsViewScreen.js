import React from 'react';
import { SafeAreaView, View, FlatList, Text } from 'react-native';
import { MainHeader } from '../../../MaterialDesign';
import ProductGridView from './ProductGridView';

const ProductsViewScreen = ({ columns, itemWidth, products, productDetailHandler }) => {

    // const navigation = useNavigation();

    //console.log('P.G.', itemWidth);

    return (
        <>
            <SafeAreaView>
                <MainHeader title={'বিজ্ঞাপন / পণ্য সমূহ'} />
                <View style={{ marginBottom: 100 }}>
                    {
                        products &&
                        <FlatList
                            numColumns={columns}
                            data={products}
                            renderItem={({ item }) => <ProductGridView itemWidth={itemWidth}
                                product={item}
                                pageName='product'
                                productDetailHandler={productDetailHandler} />}
                            // keyExtractor={(index) => index}
                                keyExtractor={(item, index) => String(index)}
                        />
                    }
                </View>
            </SafeAreaView>

        </>
    );
};

export default ProductsViewScreen;