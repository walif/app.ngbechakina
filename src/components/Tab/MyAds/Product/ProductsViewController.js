import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import ProductsViewScreen from './ProductsViewScreen';
import { getProducts,getProductsByCategoryIdServer, closeProductModel } from '../../../../store/actions/productAction';
import { LoaderModalController } from '../../../MaterialDesign';

const pageSize = 10;
const columns = 2;
const ITEM_WIDTH = Dimensions.get('window').width;

const ProductsViewController = ({ route }) => {

    const navigation = useNavigation();

    const [currentPage, setCurrentPage] = useState(1);
    const [products, setProducts] = useState([]);

    const productState = useSelector(state => state.product);
    const dispatch = useDispatch();

    useEffect(() => {
        setProducts([]);
    },[])

    useEffect(() => {
        setProducts([]);
        if (route.params.category === 'all') {
            dispatch(getProducts(pageSize, currentPage));
        }
        else {
            //console.log(route.params.category);
            dispatch(getProductsByCategoryIdServer(route.params.category,pageSize, currentPage));
        }

        ///console.log(route.params.category);
        //dispatch(getProductByProductId());
        //setProduct(route.params.product);
    }, [route]);

    useEffect(() => {
        // console.log('State:', [...products]);
        // console.log('Props:', [...productState.products]);
        setProducts([...products, ...productState.products]);
    }, [productState.products])

    useEffect(() => {
        // console.log('State:', [...products]);
        // console.log('Props:', [...productState.products]);
        setProducts([...products, ...productState.filterProducts]);
    }, [productState.filterProducts])

    const productDetailHandler = (product) => {
        navigation.navigate('ProductDetails', { product: product });
    }

    const closeModal = (returnValue) => {        
        dispatch(closeProductModel());
    }

    //console.log(productState);

    return (
        <>
            <LoaderModalController
                visible={productState.showModal}
                imageType={productState.loaderImageType}
                content={productState.loaderMessage}
                isLoop={productState.loaderLoop}
                closeModal={closeModal} />

            <ProductsViewScreen
                columns={columns}
                itemWidth={(ITEM_WIDTH - 20 * columns) / columns}
                products={products}
                productDetailHandler={productDetailHandler} />
        </>
    );
};

export default ProductsViewController;