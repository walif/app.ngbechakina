import React from 'react';
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { IMAGE } from '../../../../assets/image';
import { COLOR } from '../../../../core';
import { useNavigation } from '@react-navigation/native'

const _ProductsView = ({ products }) => {

    const navigation = useNavigation();

    return (
        products && products.map((product, index) => {
            let productImage = JSON.parse(product.productImages);
            return (
                <TouchableOpacity
                    key={index}
                    style={styles.container}
                    onPress={() => navigation.navigate('MyAds', {
                        screen: 'ProductDetails',
                        params: { product },
                    })}>
                    <Image source={productImage[0] === '' || productImage[0] === null ? IMAGE.DEFAULT_PRODUCT : { uri: productImage[0] }}
                        style={styles.img} />
                    <Text style={{ fontSize: 12, paddingHorizontal: 5 }}>{product.productTitle.length >= 45 ? product.productTitle.substr(0, 40) + '...' : product.productTitle}</Text>
                    <Text style={{ fontSize: 11, fontWeight: 'bold' }}>{product.priceAsDiscussion === false ? '৳. ' + product.productPrice : 'আলোচনা সাপেক্ষে'}</Text>

                </TouchableOpacity>
            )
        })
    )
}

export default _ProductsView;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        width: 160,
        height: 190,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        width: 150,
        height: 130,
        borderWidth: 1,
        borderColor: COLOR.COLOR_GREEN,
        resizeMode: 'contain'
    }
})