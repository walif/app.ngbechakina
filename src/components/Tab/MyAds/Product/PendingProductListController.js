import React, { useEffect } from 'react';
import { LoaderModalController } from '../../../MaterialDesign';
import ProductListScreen from './ProductListScreen';
import { getPendingProducts, closeProductModel, approvedProduct, cancelProduct } from '../../../../store/actions/productAction';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

const PendingProductListController = () => {

    const navigation = useNavigation();

    /*=====================
    REDUX STATE
    ======================*/

    const authState = useSelector(state => state.auth);
    const { showModal, loaderImageType, loaderMessage, loaderLoop, products } = useSelector(state => state.product);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPendingProducts());
    }, [])

    const closeModal = (returnValue) => {
        dispatch(closeProductModel());
    }

    const cancelProductHandler = (product) => {
        dispatch(cancelProduct(product));
    }

    const approvedProductHandler = (product) => {
        dispatch(approvedProduct(product));
    }

    const productDetailHandler = (product) => {
        // console.log(product);
        navigation.navigate('ProductDetails', { product });
    }
    //console.log('Show Modal:',showModal);

    return (
        <>
            <LoaderModalController
                visible={showModal}
                imageType={loaderImageType}
                content={loaderMessage}
                isLoop={loaderLoop}
                closeModal={closeModal} />

            <ProductListScreen
                title="পেন্ডিং বিজ্ঞাপন সমূহ"
                products={products}
                user={authState.user}
                cancelProductHandler={cancelProductHandler}
                approvedProductHandler={approvedProductHandler}
                productDetailHandler={productDetailHandler} />
        </>
    );
};

export default PendingProductListController;