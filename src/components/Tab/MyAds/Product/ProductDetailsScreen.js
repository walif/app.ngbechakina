import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, Text, Image } from 'react-native';
// import { FlatListSlider } from 'react-native-flatlist-slider';
import Swiper from 'react-native-swiper';
import moment from 'moment';
import { RVText } from '../../../../core';
import { MainHeader } from '../../../MaterialDesign';

const ProductDetailsScreen = ({
    product,
    images
}) => {

    // const images = JSON.parse(product.productImages);
    //console.log(images);

    return (
        <>
            <SafeAreaView style={styles.container} >
                <MainHeader title={'বিস্তারিত'} />
                <ScrollView
                    style={styles.container}
                    showsVerticalScrollIndicator={false}>
                    {
                        images && images.length > 0 && (
                            <View style={[styles.sliderContainer]}>
                                <Swiper
                                    style={styles.wrapper}
                                    showsButtons={false}
                                    autoplay={true}
                                    autoplayTimeout={5}
                                    bounces={false}>
                                    {
                                        images && images.map((image, index) => {
                                            return (
                                                <Image
                                                    key={index}
                                                    source={{ uri: image }}
                                                    style={styles.imageSlider} />
                                            )
                                        })
                                    }
                                </Swiper>

                                {/* <View>
                                    <FlatListSlider
                                        data={images}
                                        timer={5000}
                                        onPress={() => { }}
                                        contentContainerStyle={{ zIndex: 1 }}
                                        indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
                                        indicatorActiveColor={'#8e44ad'}
                                        indicatorInActiveColor={'#ffffff'}
                                        indicatorActiveWidth={30}
                                        animation
                                    /> 
                                </View> */}
                            </View>
                        )
                    }

                    <View style={styles.content}>
                        <View style={styles.contentInfo}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{product.productTitle}</Text>
                            <Text style={{ fontSize: 12, color: '#a0a0a0', marginVertical: 5 }}>{'পোস্টকৃতঃ ' + moment(product.approvedDateTime).format('DD MMMM,YYYY HH:mm A')}</Text>
                            <Text style={{ fontSize: 12, color: '#a0a0a0' }}>{product.customer.address.address}</Text>
                            <Text style={{ fontSize: 12, color: '#a0a0a0', fontWeight: 'bold', marginTop: 5 }}>{product.priceAsDiscussion === false ? '৳. ' + product.productPrice : 'আলোচনা সাপেক্ষে'}</Text>
                        </View>
                        <View style={styles.contentInfo}>
                            <RVText content="বিক্রেতার তথ্যঃ" style={{ fontSize: 14, fontWeight: 'bold' }} />
                            <Text style={{ fontSize: 12, color: '#a0a0a0', marginTop: 5 }}>{product.customer.name}</Text>
                            <Text style={{ fontSize: 12, color: '#a0a0a0', marginVertical: 5 }}>{JSON.parse(product.customer.mobile)}</Text>
                            <Text style={{ fontSize: 12, color: '#a0a0a0' }}>{product.customer.email}</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <RVText content="বিজ্ঞাপন / পণ্যের বিবরণঃ" style={{ fontSize: 14, fontWeight: 'bold' }} />
                            <Text style={{ fontSize: 12, color: '#a0a0a0', marginTop: 5 }}>{product.productDescription}</Text>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default ProductDetailsScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    sliderContainer: {
        flexDirection: 'column',
        flex: 3
    },
    imageSlider: {
        width: '100%',
        height: '100%',
        flex: 1,
        resizeMode: "cover"
    },
    content: {
        paddingHorizontal: 10
    },
    contentInfo: {
        borderColor: '#a0a0a0',
        borderBottomWidth: 1,
        paddingVertical: 10
    }
})