import { Input, Item, Label, Textarea } from 'native-base';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { RVText } from '../../../../core';

const ProductInfoScreen = ({
    product,
    productTitleHandler,
    productDescriptionHandler,
    productPriceHandler,
    priceAsDiscussionHandler,
}) => {
    //console.log(product);
    return (
        <>
            <View style={styles.container}>
                <RVText
                    content={'বিজ্ঞাপনের তথ্যাবলী'}
                    style={{ fontSize: 16, color: '#000' }} />
            </View>
            <View style={styles.content}>
                <View style={styles.inputView}>
                    <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>বিজ্ঞাপনের শিরোনাম</Label>
                        <Input
                            onChangeText={productTitleHandler}
                            value={product.productTitle}
                            style={{ fontSize: 12 }} />
                    </Item>
                </View>

                <View style={styles.inputView}>
                    <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>মূল্য</Label>
                        <Input
                            disabled={product.priceAsDiscussion === true ? true : false}
                            onChangeText={productPriceHandler}
                            value={product.productPrice}
                            style={{ fontSize: 12 }}
                            keyboardType="number-pad" />
                    </Item>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 10, alignItems: 'center' }}>
                    <CheckBox
                        // disabled={false}
                        value={product.priceAsDiscussion}
                        onValueChange={priceAsDiscussionHandler}
                    />
                    <RVText content="আলোচনা সাপেক্ষে" style={{ fontSize: 12 }} />
                </View>

                <View style={styles.inputView}>
                    {/* <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>বিজ্ঞাপন / পণ্যের বিবরণ</Label>
                        <Input
                            onChangeText={productDescriptionHandler}
                            value={product.productDescription}
                            style={{ fontSize: 12 }} />
                    </Item> */}
                    <Label style={{ fontSize: 14 }}>বিজ্ঞাপন / পণ্যের বিবরণ</Label>
                    <Textarea
                        rowSpan={5}
                        bordered
                        style={{ fontSize: 12 }}
                        value={product.productDescription}
                        onChangeText={productDescriptionHandler}/>
                </View>
            </View>
        </>
    );
};

export default ProductInfoScreen;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 20,
        backgroundColor: '#e7e7d5',
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#e7e7d8'
    },
    content: {
        paddingVertical: 20,
        paddingHorizontal: 20,
        backgroundColor: '#fff'
    },
    inputView: {
        marginVertical: 10
    }
})