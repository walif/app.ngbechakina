import { Left, ListItem, Right } from 'native-base';
import React from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { RVText } from '../../../../core';


const _CategoryView = ({ categories, navigateCategory }) => (

    categories && categories.map((cat, index) => {
        return (
            <ListItem key={index} button={true} onPress={() => navigateCategory(cat)}>
                <Left>
                    <RVText content={cat.categoryName} style={{ fontSize: 14 }} />
                </Left>
                <Right>
                    {
                        cat.hasChildCategory.length > 0 &&
                        <MaterialIcons name='keyboard-arrow-down' size={20} />
                    }
                </Right>
            </ListItem>
        )
    })
);

export default _CategoryView;