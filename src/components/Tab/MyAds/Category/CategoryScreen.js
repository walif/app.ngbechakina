import React from 'react';
import { View, SafeAreaView, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native';
import { List, ListItem, Left, Right, Icon } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { useNavigation } from '@react-navigation/native';
import { RVText } from '../../../../core';


// const ParentCategory = ({ parentCat, navigateParentCategory }) => (
    
//     parentCat && parentCat.map((cat, index) => {
//             return (
//                 <ListItem key={index} button={true} onPress={() => navigateParentCategory(cat)}>
//                     <Left>
//                         <RVText content={cat.categoryName} style={{ fontSize: 14 }} />
//                     </Left>
//                     {
//                         cat.hasChildCategory.length > 0 && 
//                         <MaterialIcons name='keyboard-arrow-down' size={20}/>
//                     }
//                 </ListItem>
//             )
//         })
// )

const CategoryScreen = ({ categories, parentCat, navigateCategory, navigateParentCategory }) => {
    //console.log('Parent Cat:', parentCat);
    const navigation = useNavigation();

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                {/* {
                    parentCat &&
                    <ParentCategory
                        parentCat={parentCat}
                        navigateParentCategory={navigateParentCategory} />
                } */}
                <List>
                    {
                        categories && categories.map((cat, index) => {
                            return (
                                <ListItem key={index} button={true} onPress={() => navigateCategory(cat)}>
                                    <Left>
                                        <RVText content={cat.categoryName} style={{ fontSize: 14 }} />
                                    </Left>
                                    {
                                        cat.hasChildCategory.length > 0 && 
                                            <MaterialIcons name='keyboard-arrow-down' size={20}/>
                                    }
                                </ListItem>
                            )
                        })
                    }
                </List>
            </ScrollView>
        </SafeAreaView>
    );
};

export default CategoryScreen;