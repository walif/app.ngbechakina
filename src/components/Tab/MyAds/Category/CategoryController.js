import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LoaderModalController } from '../../../MaterialDesign';
import CategoryScreen from './CategoryScreen';
import { loadCategories, closeModel } from '../../../../store/actions/categoryAction';

const CategoryController = () => {

    const [categoryList,setCategoryList] = useState([]);
    const [parentCat,setParentCat] = useState([]); 
    const { showModal, loaderImageType, loaderMessage, loaderLoop, categories, timestamp } = useSelector(state => state.category);
    const dispatch = useDispatch();

    //console.log(categories);

    useEffect(() => {
        setParentCat([]);
        dispatch(loadCategories());
    }, []);


    useEffect(() => {
        setCategoryList(categories);
    }, [categories,timestamp]);


    const navigateCategory = (category) => {
        setCategoryList(category.hasChildCategory);
        setParentCat([...parentCat,category]);
        //console.log(parentCat);
    }

    const navigateParentCategory = (category) => {
        //console.log(parentCat);
        if(category.parentCategoryId === 0) {
            setParentCat([]);
            setCategoryList(categories);
        }
        else {
            let child = parentCat.find(c=>c.categoryId === category.parentCategoryId);
            let parent = parentCat.filter(c=>c.categoryId !== category.categoryId);
            // console.log(child);
            // console.log(parent);
            setParentCat(parent);
            setCategoryList(child.hasChildCategory);
        }
        
        //if(parentCat.parentCat)
    }



    const closeModal = (returnValue) => {
        dispatch(closeModel());
        //console.log(returnValue);
        // this.setState({
        //   ...this.state,
        //   showModal: false
        // });
    }

    return (
        <>
            {
                loaderImageType && loaderMessage && (
                    <LoaderModalController
                        visible={showModal}
                        imageType={loaderImageType}
                        content={loaderMessage}
                        isLoop={loaderLoop}
                        closeModal={closeModal} />
                )

            }


            <CategoryScreen
                categories={categoryList}
                parentCat={parentCat}
                navigateCategory={navigateCategory}
                navigateParentCategory={navigateParentCategory} />
        </>
    );
};

export default CategoryController;