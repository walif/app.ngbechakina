import { Button } from 'native-base';
import React from 'react';
import { SafeAreaView, View,StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { RVText } from '../../../core';
import { MainHeader } from '../../MaterialDesign';

const MyAdsScreen = ({ user }) => {

    const navigation = useNavigation();

    console.log('Hello world!');

    const loadClientContent = () => (
        <View style={styles.container}>
            <Button block style={{ marginBottom: 10 }} onPress={() => navigation.navigate('Product')}>
                <RVText content='আপনার বিজ্ঞাপন পোস্ট করুন' style={{ fontSize: 14, color: '#fff' }} />
            </Button>
            <Button block onPress={() => navigation.navigate('CustomerProductList')}>
                <RVText content='আপনার বিজ্ঞাপন সমূহ' style={{ fontSize: 14, color: '#fff' }} />
            </Button>
        </View>

    )

    const loadAdminContent = () => (
        <View style={styles.container}>
            <Button block style={{ marginBottom: 10 }} onPress={() => navigation.navigate('PendingProductList')}>
                <RVText content='পেন্ডিং বিজ্ঞাপন সমূহ' style={{ fontSize: 14, color: '#fff' }} />
            </Button>
            <Button block style={{ marginBottom: 10 }} onPress={() => navigation.navigate('ApprovedProductList')}>
                <RVText content='অনুমোদিত বিজ্ঞাপন সমূহ' style={{ fontSize: 14, color: '#fff' }} />
            </Button>
            {/* <Button block onPress={() => navigation.navigate('CustomerProductList')}>
                <RVText content='বাতিল বিজ্ঞাপন সমূহ' style={{ fontSize: 14, color: '#fff' }} />
            </Button> */}
        </View>
    )

    return (
        <>
            <SafeAreaView style={{ flex: 1 }}>
                <MainHeader title={'বিজ্ঞাপন প্যানেল'} />
                {
                    user && user.userType === 'admin' ? loadAdminContent() : loadClientContent()
                }
            </SafeAreaView>
        </>
    );
};

export default MyAdsScreen;

const styles = StyleSheet.create({
    container: { 
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        marginHorizontal: 10, 
        marginVertical: 10 
    }
})