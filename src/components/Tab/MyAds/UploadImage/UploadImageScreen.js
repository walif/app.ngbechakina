import React from 'react';
import { View, StyleSheet, ScrollView, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { Button } from 'native-base';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { COLOR, RVText } from '../../../../core';

const UploadImageScreen = ({ imagesUrl, uploadHandler, deleteUploadImageHandler }) => {
    //console.log('Upload Images:', imagesUrl);
    return (
        <>
            <View style={styles.container}>
                <RVText
                    content={'কমপক্ষে একটি ছবি যুক্ত করুন (ম্যাক্স ৫)'}
                    style={{ fontSize: 16, color: '#000' }} />
            </View>
            <View style={styles.content}>
                {
                    imagesUrl && imagesUrl.length > 0 ? (
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            {
                                imagesUrl.map((img, index) => {
                                    return (
                                        <ImageBackground
                                            key={index}
                                            source={{ uri: img.uri }}
                                            style={styles.uploadContent}>
                                            <TouchableOpacity
                                                onPress={() => deleteUploadImageHandler(img)}
                                                style={{ position: 'absolute', top: -12, right: -10,padding:5 }}>
                                                <IconFA name="minus-circle"
                                                    size={20}
                                                    color="#b36660" />
                                            </TouchableOpacity>
                                        </ImageBackground>
                                    )
                                })
                            }
                        </ScrollView>

                    ) : (
                        <IconFA name='camera'
                            size={50}
                            color={COLOR.ICON_PRIMARY_COLOR}
                        />
                    )
                }

                <Button block
                    style={[{
                        backgroundColor: '#00aaff',
                        marginHorizontal: 10,
                        marginTop: 20
                    }]}
                    onPress={uploadHandler}>
                    <RVText content={'ছবি যুক্ত করুন'} style={{ fontSize: 16, color: '#fff' }} />
                </Button>
            </View>

        </>

    );
};

export default UploadImageScreen;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 20,
        backgroundColor: '#e7e7d5',
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#e7e7d8'
    },
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        backgroundColor: '#fff'
    },
    uploadContent: {
        width: 80,
        height: 80,
        backgroundColor: '#d1d1d1',
        borderColor: '#ccc',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        marginBottom: 10,
        marginRight: 15
    }
})