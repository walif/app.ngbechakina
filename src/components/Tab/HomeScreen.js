import React, { Component } from 'react';
import {
  View, SafeAreaView, TouchableOpacity, Image,
  StyleSheet, Alert, ActivityIndicator, Linking
} from 'react-native';
import { connect } from 'react-redux';
import { 
  NetInfoSubscriptionController,
  CustomHeader,
  CustomMessageBox
} from '../MaterialDesign';
import { SITE } from '../../core';

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    }
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      isLoading: false
    })
  }

  underConstruction = () => {
    //console.log(this.state);
    Alert.alert(
      'Feature notifications',
      'This feature release as soon as possible',
      [
        { text: 'OK' }
      ],
      { cancelable: false }
    );
  }
  openionHandler = () => {
    Alert.alert(
      'Openion',
      'Inbox your query, complain or suggestion on your facebook page',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'OK', onPress: () => {
            Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err));
          }
        }
      ],
      { cancelable: false }
    );
  }

  bannerHandler = () => {
    this.props.navigation.navigate('Balance')
    // Alert.alert(
    //   'Rate my12class',
    //   'Are you want to rate this app?',
    //   [
    //     {
    //       text: 'Cancel',
    //       style: 'cancel'
    //     },          
    //     { text: 'OK',onPress: ()=> {
    //         Linking.openURL(SITE.rateApp).catch((err) => console.error('An error occurred', err));
    //       } 
    //     }
    //   ],
    //   { cancelable: false }
    // );
  }

  render() {
    let { navigation } = this.props;
    return (
      <SafeAreaView style={{ flex: 1 }}>

        <NetInfoSubscriptionController />

        <CustomHeader title='My12Class' isHome={true} navigation={navigation} />
        <View style={{ flex: 1 }}>
          {
            this.state.isLoading == true ?
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: "#f3eeee" }}>
                <ActivityIndicator size="large"></ActivityIndicator>
              </View>
              :
              <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/subjective_preparation.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/question_bank_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/university_info_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/Archeive_exam_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/live_exam_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/admission_info_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/result_icon.png')} style={[styles.image, { width: 150 }]} />
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/exam_schedule_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/helptips.png')} style={[styles.image, { width: 70 }]} />
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{ flexDirection: 'row', justifyContent: "center", height: 60, marginTop: 0, alignItems: 'center' }}>
                  <TouchableOpacity onPress={() => { Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err)); }}>
                    <Image source={require('../../assets/images/cash_back.jpg')} style={styles.banner} />
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/online_form_icon.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/smart_bee.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <TouchableOpacity style={styles.contentWrapper} onPress={() => this.underConstruction()}>
                      <Image source={require('../../assets/images/lets_discuss.png')} style={styles.image} />
                    </TouchableOpacity>
                  </View>

                </View>

                <CustomMessageBox />
              </View>
          }
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.auth
})
export default connect(mapStateToProps, null)(HomeScreen);

const styles = StyleSheet.create({
  contentWrapper: {
    //borderWidth: 1,
    //borderRadius: 1,
    //borderColor: '#ddd',
    //borderBottomWidth: 0,
    //shadowColor: '#000',
    //shadowOffset: { width: 0, height: 2 },
    //shadowOpacity: 0.8,
    //shadowRadius: 2,
    //elevation: 1,
    marginLeft: 2,
    marginRight: 2,
    padding: 5,
    //backgroundColor:"#fff",
  },
  image: {
    width: 95,
    height: 85,
    resizeMode: 'contain',
    //borderColor:'#ccc',
    //borderWidth:1
  },
  banner: {
    maxWidth: '98%',
    resizeMode: 'contain',
    marginLeft: 3
  }
})