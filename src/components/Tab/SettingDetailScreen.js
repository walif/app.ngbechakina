import React,{ Component } from 'react';
import {  View,  Text,SafeAreaView } from 'react-native';
import { CustomHeader } from '../index';
import { RVText } from '../core';

export class SettingsDetailScreen extends Component {
    render() { 
      let {navigation} = this.props;
        return (
            <SafeAreaView style={{ flex: 1 }}>
              <CustomHeader title='Setting Detail' navigation = {navigation}/>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <RVText content='Setting Detail' />
              </View>      
            </SafeAreaView>
          )
    }
}