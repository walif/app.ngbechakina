import React, { useEffect, useState } from 'react';
import { Text, Image, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { SLIDER } from '../../../assets/slider';
import HomeScreen from './HomeScreen';
import { LoaderModalController, SimpleModalController } from '../../MaterialDesign';
import { getProducts, closeProductModel } from '../../../store/actions/productAction';

const win = Dimensions.get('window');
const ratio = win.width / 541; //541 is actual image width

// const images = [
//   {
//     image: SLIDER.IMAGE_001,
//     desc: 'Silent Waters in the mountains in midst of Himilayas',
//   },
//   {
//     image: SLIDER.IMAGE_002,
//     desc: 'Red fort in India New Delhi is a magnificient masterpeiece of humans',
//   },
//   {
//     image: SLIDER.IMAGE_003,
//     desc: 'Red fort in India New Delhi is a magnificient masterpeiece of humans',
//   },
//   {
//     image: SLIDER.IMAGE_004,
//     desc: 'Red fort in India New Delhi is a magnificient masterpeiece of humans',
//   },
// ]

const HomeController = () => {

  const navigation = useNavigation();

  const [showAdsModal, setAdsModal] = useState(false);
  const [adsImage, setAdsImage] = useState(null);

  const { showModal, loaderImageType, loaderMessage, loaderLoop, products } = useSelector(state => state.product);
  const dispatch = useDispatch();

  useEffect(() => {
    const pageSize = 10;
    const pageNumber = 1;
    dispatch(getProducts(pageSize, pageNumber));
  }, [])

  const showAds = (uri) => {
    // console.log(uri);
    setAdsImage(uri);
    setAdsModal(true);
  }

  const closeModal = (redirectUri) => {
    // console.log(redirectUri);
    setAdsModal(false)
  }

  const dismisModal = (redirectUri) => {
    dispatch(closeProductModel());
  }
  //console.log(products);


  //When press single producut then redirect to product single /detail page

  const productDetailHandler = (product) => {
    navigation.navigate('MyAds', {
      screen: 'ProductDetails',
      params: { product },
    });
  }

  const topProductHandler = () => {
    navigation.navigate('MyAds', {
      screen: 'ProductsView',
      params: { category: 'all' },
    })
  }

  const productCategoryHandler = (categoryId) => {
    navigation.navigate('MyAds', {
      screen: 'ProductsView',
      params: { category: categoryId },
    })
  }

  return (
    <>
      {/* SHOW MODAL */}
      <SimpleModalController
        visible={showAdsModal}
        closeModal={closeModal}
        closeIcon={true}>

        <Image source={adsImage}
          style={{
            width: '100%',
            height: 562 * ratio,
            resizeMode: 'contain'
          }} />

      </SimpleModalController>

      <LoaderModalController
        visible={showModal}
        imageType={loaderImageType}
        content={loaderMessage}
        isLoop={loaderLoop}
        closeModal={dismisModal} />

      <HomeScreen
        //images={images}
        showAds={showAds}
        products={products}
        productDetailHandler={productDetailHandler}
        topProductHandler={topProductHandler}
        productCategoryHandler={productCategoryHandler} />
    </>
  );
};

export default HomeController;