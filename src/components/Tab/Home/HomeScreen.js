import React from 'react';
import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { MainLayout } from '../../MaterialDesign';
// import { FlatListSlider } from 'react-native-flatlist-slider';
import Swiper from 'react-native-swiper';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontisto from 'react-native-vector-icons/Fontisto';
import IconEntypo from 'react-native-vector-icons/Entypo';
import { RVText } from '../../../core';
import FocusAwareStatusBar from '../../../core/FocusAwareStatusBar';
import { ADS } from '../../../assets/ads';
import { BRAND } from '../../../assets/brand';
import LinearBackground from '../../MaterialDesign/Background/LinearBackground';
import { COLOR } from '../../../core';
import { ProductGridView } from '../MyAds';
import { SLIDER } from '../../../assets/slider';


const LoadVendorContentScreen = () => (
    <>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_001}
                style={styles.vendorImg} />
        </TouchableOpacity>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_002}
                style={styles.vendorImg} />
        </TouchableOpacity>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_003}
                style={styles.vendorImg} />
        </TouchableOpacity>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_004}
                style={styles.vendorImg} />
        </TouchableOpacity>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_005}
                style={styles.vendorImg} />
        </TouchableOpacity>
        <TouchableOpacity>
            <Image
                source={BRAND.BRAND_006}
                style={styles.vendorImg} />
        </TouchableOpacity>
    </>
)

const LoadSpecialAdsContentScreen = ({ showAds }) => (
    <>
        <TouchableOpacity
            style={styles.specialAdsContainer}
            onPress={() => showAds(ADS.IMAGE_001)}>
            <Image
                source={ADS.IMAGE_THUMB_001}
                style={styles.specialAdsImg} />
        </TouchableOpacity>
        <TouchableOpacity
            style={styles.specialAdsContainer}
            onPress={() => showAds(ADS.IMAGE_002)}>
            <Image
                source={ADS.IMAGE_002}
                style={styles.specialAdsImg} />
        </TouchableOpacity>
        <TouchableOpacity
            style={styles.specialAdsContainer}
            onPress={() => showAds(ADS.IMAGE_003)}>
            <Image
                source={ADS.IMAGE_003}
                style={styles.specialAdsImg} />
        </TouchableOpacity>
        <TouchableOpacity
            style={styles.specialAdsContainer}
            onPress={() => showAds(ADS.IMAGE_004)}>
            <Image
                source={ADS.IMAGE_004}
                style={styles.specialAdsImg} />
        </TouchableOpacity>
    </>
)

const HomeScreen = ({
    //images,
    showAds,
    products,
    productDetailHandler,
    topProductHandler,
    productCategoryHandler }) => {



    return (
        <MainLayout>
            <FocusAwareStatusBar translucent backgroundColor='transparent' />
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>

                <View style={[styles.sliderContainer]}>
                    {/* <View> */}

                        <Swiper
                            style={styles.wrapper}
                            showsButtons={false}
                            autoplay={true}
                            autoplayTimeout={5}>
                            {/* <View style={styles.slide1}> */}
                            <Image
                                source={SLIDER.IMAGE_001}
                                style={styles.imageSlider} />

                            <Image
                                source={SLIDER.IMAGE_002}
                                style={styles.imageSlider} />

                            <Image
                                source={SLIDER.IMAGE_003}
                                style={styles.imageSlider} />

                            <Image
                                source={SLIDER.IMAGE_004}
                                style={styles.imageSlider} />
                        </Swiper>

                        {/* <FlatListSlider
                            data={images}
                            timer={5000}
                            imageKey={'image'}
                            local
                            onPress={() => { }}
                            // contentContainerStyle={{paddingHorizontal: 16}}
                            indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
                            indicatorActiveColor={'#8e44ad'}
                            indicatorInActiveColor={'#ffffff'}
                            indicatorActiveWidth={30}
                            animation
                        /> */}
                    {/* </View> */}
                </View>
                <LinearBackground>
                    <View style={[{ flex: 1 }]}>
                        <View style={{ marginHorizontal: 15, marginVertical: 50, position: 'relative' }}>
                            <View style={styles.categoryContent}>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity style={[styles.catItemStyle]}
                                        onPress={() => productCategoryHandler(1)}>
                                        <IconFontisto name="island" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="জমি বেচা কিনা" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.catItemStyle]}
                                        onPress={() => productCategoryHandler(1)}>
                                        <IconMC name="home-city" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="ফ্ল্যাট ভাড়া বিক্রি" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.catItemStyle, { marginTop: 8 }]}
                                        onPress={() => productCategoryHandler(52)}>
                                        <IconEntypo name="shop" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <Text style={styles.textCatStyle}>
                                            দোকান ভাড়া {"\n"}বিক্রি
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity style={styles.catItemStyle}
                                        onPress={() => productCategoryHandler(22)}>
                                        <IconFontAwesome5 name="desktop" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="ইলেকট্রনিক্স বিক্রি" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.catItemStyle]}
                                        onPress={() => productCategoryHandler(34)}>
                                        <IconFontAwesome5 name="car-alt" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="গাড়ি ভাড়া বিক্রি" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.catItemStyle]}
                                        onPress={() => productCategoryHandler(53)}>
                                        <IconMC name="table-furniture" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="ফার্নিচার বিক্রি" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity style={[styles.catItemStyle, { marginTop: 5 }]}
                                        onPress={() => productCategoryHandler(54)}>
                                        <IconFA name="line-chart" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="সকল প্রতিষ্ঠানের বিঙ্গাপন" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.catItemStyle]}
                                        onPress={() => productCategoryHandler(55)}>
                                        <IconMC name="podium-gold" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <RVText content="জুয়েলারি বিক্রি" style={styles.textCatStyle} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.catItemStyle, { marginTop: 5 }]}
                                        onPress={() => productCategoryHandler(51)}>
                                        <IconEntypo name="tools" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
                                        <Text style={styles.textCatStyle}>
                                            মেশিনারিজ ভাড়া {"\n"}বিক্রি
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ position: 'absolute', bottom: -15, left: '45%' }}>
                                    <IconAntDesign name="pluscircle" size={35} color={COLOR.ICON_PRIMARY_COLOR} />
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={styles.adsContainer}>
                        <View style={{ flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#ccc' }}>
                            <RVText content='বিশেষ বিঙ্গাপনসমূহ' style={{ flex: 1, fontSize: 12, fontWeight: 'bold' }} />
                            <IconAntDesign name="caretup" style={{ flex: 1, textAlign: 'right' }} />
                        </View>
                        <ScrollView
                            contentContainerStyle={{ marginVertical: 10 }}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            <LoadSpecialAdsContentScreen showAds={showAds} />
                        </ScrollView>
                    </View>

                    <View style={[styles.adsContainer, { marginVertical: 20, marginBottom: 20 }]}>
                        <View style={{ flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#ccc' }}>
                            <Text style={{ flex: 1, fontSize: 12, fontWeight: 'bold', justifyContent: 'center' }}>টপ বিঙ্গাপনসমূহ</Text>
                            <TouchableOpacity
                                style={styles.caretWrapper}
                                onPress={topProductHandler}>
                                <IconAntDesign name="caretup" style={{ flex: 1, textAlign: 'right' }} />
                            </TouchableOpacity>

                        </View>
                        <ScrollView
                            contentContainerStyle={{ marginVertical: 10 }}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            {/* <_ProductsView products={products} /> */}
                            {
                                products && products.map((product, index) => <ProductGridView key={index}
                                    productDetailHandler={productDetailHandler}
                                    itemWidth={160}
                                    product={product} />)
                            }
                        </ScrollView>
                    </View>

                    <View style={[styles.vendorContainer, { marginBottom: 20 }]}>
                        <View style={{ flexDirection: 'row', paddingVertical: 5, paddingHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#ccc' }}>
                            <RVText content='সম্মানীত বিঙ্গাপনদাতাগণ' style={{ flex: 1, fontSize: 12, fontWeight: 'bold' }} />
                            <IconAntDesign name="caretup" style={{ flex: 1, textAlign: 'right' }} />
                        </View>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            <LoadVendorContentScreen />
                        </ScrollView>
                    </View>

                </LinearBackground>
            </ScrollView>
        </MainLayout>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
    borderLayout: {
        borderWidth: 1,
        borderColor: 'red'
    },
    sliderContainer: {
        flex: 3,
        flexDirection: 'column',
        height:250
    },
    imageSlider: {
        width: '100%',
        height: 240,
        flex: 1,
        resizeMode: "stretch"
    },
    contentContainer: {
        flex: 4,
        flexDirection: 'column'
    },
    categoryContent: {
        // position: 'absolute',
        width: '100%',
        height: 200,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    catItemStyle: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    adsContainer: {
        flex: 2,
        marginBottom: 5,
        marginHorizontal: 0,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    vendorContainer: {
        flex: 1,
        marginVertical: 5,
        marginHorizontal: 0,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    vendorImg: {
        width: 80,
        height: 50,
        resizeMode: 'contain'
    },
    textCatStyle: {
        fontSize: 11,
        textAlign: 'center'
    },
    specialAdsContainer: {
        width: 150,
        height: 130,
        justifyContent: 'center',
        alignItems: 'center'
        // borderWidth: 1,
        // borderColor: 'red'
    },
    specialAdsImg: {
        width: 140,
        height: 120,
        resizeMode: 'stretch',
        // borderWidth: 1,
        // borderColor: 'green'
    },
    caretWrapper: {
        // borderWidth: 1,
        // borderColor: 'red',
        paddingVertical: 5,
        paddingHorizontal: 5
    }
})