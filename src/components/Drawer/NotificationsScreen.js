import React,{ Component } from 'react';
import {  View,  Text,SafeAreaView } from 'react-native';
import { CustomHeader } from '../MaterialDesign';
import { RVText } from '../../core';

export class NotificationsScreen extends Component {
    render() { 
      let {navigation} = this.props;
        return (
            <SafeAreaView style={{ flex: 1 }}>
              <CustomHeader title='Notification' navigation = {navigation}/>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <RVText content='Notifications!' />
              </View>      
            </SafeAreaView>
          )
    }
}