import AboutScreen from './AboutScreen';
import CopyRightScreen from './CopyRightScreen';
import NotificationsScreen from './NotificationsScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import CustomDrawerContent from './CustomDrawerContent';

export {
    AboutScreen,
    CopyRightScreen,
    NotificationsScreen,
    PrivacyPolicyScreen,
    CustomDrawerContent
}