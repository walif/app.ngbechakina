import React, { Component } from 'react';
import { View, SafeAreaView, ActivityIndicator, StyleSheet, ScrollView, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CustomHeader, CustomMessageBox } from '../MaterialDesign';

class AboutScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  async componentDidMount() {

    let token = await AsyncStorage.getItem('token');
    if (token == null) {
      nextProps.navigation.navigate('Login');
    }
    this.setState({
      ...this.state,
      isLoading: false
    });
  }

  render() {
    let { navigation } = this.props;
    let { isLoading } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader isHome={true} title='About Us' navigation={navigation} />
        <View style={{ flex: 1 }}>
          <View style={{ padding: 15 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Text style={styles.textStyle}>
                NGbechakena.com – The Largest Marketplace Narayanganj in Bangladesh. Online Selling, Buying & Give Advertising.
              </Text>
              <Text style={styles.textStyle}>
                Download the official free NGbechakena app on your phone for easy access to the largest classifieds marketplace
                Narayanganj in Bangladesh where you can buy, sell and search for a wide range of products, all on the go! Buy and
                sell anything from cars, motorcycles and other vehicles to mobile phones, laptops, TVs, cameras and much more.
                Search for property for rent & sale or apply for jobs narayanganj in Bangladesh or jobs overseas, all on NGbechakena.
              </Text>
              <Text style={[styles.textStyle, { fontSize: 18, fontWeight: 'bold' }]}>
                Shop in categories like:
              </Text>

              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>
                Electronics
              </Text>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Buy and sell brand new and used mobile phones, tablets, laptops,computer, cameras, TVs, video games, consoles, audio gear, lighting,fan,air conditions </Text>
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Compare prices</Text>
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}> Find accessories like cases, headphones, adapters and more </Text>
              </View>

              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>
                Cars & Vehicles
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Buy and sell brand new and used cars, motorcycles, scooters, trucks, buses, vans, auto rikshwa and even boats     and heavy duty vehicles</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Compare prices and search for vehicles from popular brands like Toyota, Honda, Nissan, Kia, • Mitsubishi, Tata, Hyundai, and more</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Filter your search by the vehicle's’ condition, model year, mileage etc</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Find parts and accessories for your vehicles</Text>
              </View>

              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>
                Property
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Buy, sell and rent your property through NGbechekena</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Search for houses, apartments, flats, land and commercial property</Text>
              </View>

              <Text style={{ fontWeight: 'bold', fontSize: 16 }}>
                Jobs
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Find jobs Narayanganj in Bangladesh and overseas with NGbechakena</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Post job offers for your company or organization</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Search for full time, part time or contractual jobs and internships</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Browse through offers in the marketing, sales, IT, administration and customer service industries</Text>
              </View>

              <Text style={[styles.textStyle, { marginTop: 10 }]}>
                With NGbechakena you can:
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Browse thousands of local ads Rupganj, Araihazar, Sonargaon, Fatulla,Bandar & Sadar Upazila District in Narayanganj.
                  On one easy-to-use app</Text>
              </View>

              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Search by category, location and keywords to find exactly what you want  </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Connect with sellers in just one tap to make great deals</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Save your favourite ads for later</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Browse</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Purchase select items from sellers directly on NGbechekena using Buy Now feature</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Promote your ads by using Bump Up and Top Ad</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Find all categories combined into the same app</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Post ads fast directly from your phone</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Manage all your posted ads in one place</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Text>{'\u2022'}</Text>
                <Text style={{ flex: 1, paddingLeft: 5 }}>Save on data usage with our data-saving mode</Text>
              </View>
              <Text style={{ paddingVertical: 20 }}>
                Download the free app now and do all of your online shopping on the largest marketplace Narayanganj in Bangladesh. Welcome!
              </Text>
              <Text style={{ paddingVertical: 10 }}>
                Want to know more about NGbechakena? Visit http://ngbechakena.com/en/help/about
              </Text>
              <Text style={{ paddingVertical: 10 }}>
                Supported Languages:{'\n'}
                English & Bangla
              </Text>

            </ScrollView>
          </View>
        </View>
        {/* <CustomHeader isHome={true} title='About Us' navigation={navigation} />
        <View style={{ flex: 1, backgroundColor: '#fff', marginBottom: 20 }}>
          {
            isLoading == true ?
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <ActivityIndicator size="large"></ActivityIndicator>
              </View>
              :
              <View style={{ flex: 1 }}>
                <View style={{ padding: 15 }}>
                  <ScrollView>
                    <Text style={styles.textStyle}>
                      NGbechakena.com – The Largest Marketplace Narayanganj in Bangladesh. Online Selling, Buying & Give Advertising.
                    </Text>
                    <Text style={styles.textStyle}>
                      Download the official free NGbechakena app on your phone for easy access to the largest classifieds marketplace
                      Narayanganj in Bangladesh where you can buy, sell and search for a wide range of products, all on the go! Buy and
                      sell anything from cars, motorcycles and other vehicles to mobile phones, laptops, TVs, cameras and much more.
                      Search for property for rent & sale or apply for jobs narayanganj in Bangladesh or jobs overseas, all on NGbechakena.
                    </Text>
                    <Text style={styles.textStyle}>
                      Shop in categories like:
                    </Text>
                  </ScrollView>
                </View>
              </View>
          }
        </View>
        <CustomMessageBox /> */}
      </SafeAreaView>
    )
  }
}
export default AboutScreen;

const styles = StyleSheet.create({
  textStyle: {
    lineHeight: 25,
    marginBottom: 15,
    textAlign: 'justify'
  }
});