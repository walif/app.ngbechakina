import React, { Component, useEffect, useState } from 'react';
import {
  View, SafeAreaView, ImageBackground, Image,
  Text, TouchableOpacity, ScrollView, Linking,
  Share, StyleSheet
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { connect, useDispatch, useSelector } from 'react-redux';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconOcticons from 'react-native-vector-icons/Octicons';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
// import AsyncStorage from '@react-native-community/async-storage';
import { IMAGE } from '../../assets/image';
import { RVText, SITE, COLOR } from '../../core';
import { logout } from '../../store/actions/authAction';

// const init = {
//   result: '',
//   isLogoutClick: false,
//   authText: 'Logout'
// }

const CustomDrawerContent = ({navigation}) => {

  const [result, setResult] = useState("");
  const [authText, setAuthText] = useState("");

  const authState = useSelector(state => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!authState.user && !authState.isAuthenticated) {
      setAuthText('Login');
      navigation.navigate('Home', {
        screen: 'Home'
      })
    }
    else {
      setAuthText('Logout');
    }
  }, [authState])

  //const navigation = useNavigation();

  const showResult = (result) => {
    setResult({ result });
    // this.setState({ result });
  }

  const shareApp = () => {
    Share.share({
      message: 'Share ngbechakina with your friends',
      url: SITE.website
    }).then(showResult)
  }

  const openWindow = (url) => {
    switch (url) {
      case 'fb':
        Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err));
        break;
      case 'fbgroup':
        Linking.openURL(SITE.fbGroup).catch((err) => console.error('An error occurred', err));
        break;
      case 'youtube':
        Linking.openURL(SITE.youtube).catch((err) => console.error('An error occurred', err));
        break;
      case 'addBalance':
        Linking.openURL(SITE.addBalance).catch((err) => console.error('An error occurred', err));
        break;
      case 'rateApp':
        Linking.openURL(SITE.rateApp).catch((err) => console.error('An error occurred', err));
        break;
      default:
        Linking.openURL(SITE.website).catch((err) => console.error('An error occurred', err));
    }
  }

  const logoutHandler = () => {

    console.log(navigation);

    if (authText === 'Login') {
      navigation.closeDrawer();
      navigation.navigate('Login');
      //console.log(navigation);
    } else {
      dispatch(logout());
    }
    //console.log(this.props);
    // this.setState({
    //   ...this.state,
    //   isLogoutClick: true
    // }, () => {
    //   this.props.logout();
    // });
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ height: 180 }}>
        <ImageBackground source={IMAGE.DRAWER_HEADER_BG} style={{ width: '100%', height: '100%', alignItems: "center", justifyContent: "center" }}>
          <Image source={IMAGE.APP_LOGO}
            style={{ width: 120, height: 120 }} />
        </ImageBackground>

      </View>
      <View style={{ marginLeft: 5 }}>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => navigation.navigate('MenuTab')}>
          <IconFontAwesome5 name="home" size={18} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Home' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => openWindow('rateApp')}>
          <IconMC name="star-half-full" size={23} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Rate Us' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => openWindow('fb')}>
          <IconFontAwesome5 name="facebook" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='FB Page' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => openWindow('fbgroup')}>
          <IconFontAwesome5 name="facebook" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='FB Group' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => openWindow('youtube')}>
          <IconMC name="youtube-tv" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Youtube' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => shareApp()}>
          <IconFeather name="share-2" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Share this App' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => navigation.navigate('About')}>
          <IconFontAwesome5 name="users" size={18} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='About Us' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => navigation.navigate('PrivacyPolicy')}>
          <IconOcticons name="law" size={24} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Privacy Policy' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => openWindow('web')}>
          <IconMC name="web" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content='Web Portal' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
          onPress={() => logoutHandler()}>
          <IconAntDesign name={authText === 'Login' ? 'login' : 'logout'} size={18} color={COLOR.ICON_PRIMARY_COLOR} />
          <RVText content={authText} style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
        </TouchableOpacity>
      </View>
      <View style={{ borderTopWidth: 2, borderTopColor: COLOR.ICON_PRIMARY_COLOR, marginHorizontal: 10, marginVertical: 20 }}>
        <View style={{ marginTop: 50, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 12 }}>App Version:1.0.0</Text>
          <Text style={{ fontSize: 12 }}>Last Update:19 August,2021</Text>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default CustomDrawerContent;


// class CustomDrawerContent extends Component {

//   constructor(props) {
//     super(props);
//     this.shareApp = this.shareApp.bind(this);
//     this.showResult = this.showResult.bind(this);

//   }

//   componentDidMount() {

//   }

//   componentDidUpdate(prevProps, prevState) {
//     console.log(this.state);
//     if (!this.props.auth.user && this.state.isLogoutClick) {
//       this.setState({
//         ...this.state,
//         authText:'Login',
//         isLogoutClick: false
//       }, () => {
//         this.props.navigation.replace('HomeApp');
//         // this.props.navigation.replace('Login');
//       });
//     }
//     // if(JSON.stringify(this.props.auth.user) != JSON.stringify(prevProps.auth.user)) {
//     //   if(!this.props.auth.user) {
//     //     this.props.navigation.navigate('Login');
//     //   }
//     // }
//   }

//   showResult = (result) => {
//     this.setState({ result });
//     //console.log(result);
//   }

//   shareApp = () => {
//     Share.share({
//       message: 'Share my12class with your friends',
//       url: SITE.website
//     }).then(this.showResult)
//   }

//   // async UNSAFE_componentWillReceiveProps(nextProps) {
//   //   // let token = await AsyncStorage.getItem('token');
//   //   // if(token == null) {
//   //   //   nextProps.navigation.navigate('Login');
//   //   // }
//   //   // console.log('Drawer',token);
//   //   // if(!nextProps.auth.isAuthenticated) {
//   //   //   nextProps.navigation.navigate('Login');
//   //   // }
//   // }

//   openWindow = (url) => {
//     switch (url) {
//       case 'fb':
//         Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err));
//         break;
//       case 'fbgroup':
//         Linking.openURL(SITE.fbGroup).catch((err) => console.error('An error occurred', err));
//         break;
//       case 'youtube':
//         Linking.openURL(SITE.youtube).catch((err) => console.error('An error occurred', err));
//         break;
//       case 'addBalance':
//         Linking.openURL(SITE.addBalance).catch((err) => console.error('An error occurred', err));
//         break;
//       case 'rateApp':
//         Linking.openURL(SITE.rateApp).catch((err) => console.error('An error occurred', err));
//         break;
//       default:
//         Linking.openURL(SITE.website).catch((err) => console.error('An error occurred', err));
//     }
//   }

//   logoutHandler = () => {
//     //console.log(this.props);
//     this.setState({
//       ...this.state,
//       isLogoutClick: true
//     }, () => {
//       this.props.logout();
//     });

//   }

//   render() {
//     let { navigation } = this.props;
//     return (
//       <SafeAreaView style={{ flex: 1 }}>
//         <View style={{ height: 180 }}>
//           <ImageBackground source={IMAGE.DRAWER_HEADER_BG} style={{ width: '100%', height: '100%', alignItems: "center", justifyContent: "center" }}>
//             <Image source={IMAGE.APP_LOGO}
//               style={{ width: 120, height: 120, borderRadius: 60, borderWidth: 5, borderColor: '#c8f0f9' }} />
//           </ImageBackground>

//         </View>
//         <View style={{ marginLeft: 5 }}>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => navigation.navigate('MenuTab')}>
//             <IconFontAwesome5 name="home" size={18} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Home' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           {/* <TouchableOpacity 
//                   style={{marginTop:20,flexDirection:'row'}}
//                   onPress={() => navigation.navigate('MenuTab')}>
//                     <IconEntypo name="open-book" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//                     <RVText content='User Guid' style={{marginLeft: 15,fontSize: 12,fontWeight:"bold"}} />
//                 </TouchableOpacity> */}

//           {/* <TouchableOpacity 
//                   style={{marginTop:20,flexDirection:'row'}}
//                   onPress={() => this.openWindow('addBalance')}>
//                     <IconMaterialIcons name="attach-money" size={23} color={COLOR.ICON_PRIMARY_COLOR} />
//                     <RVText content='How to Load Balance' style={{marginLeft: 15,fontSize: 12,fontWeight:"bold"}} />
//                 </TouchableOpacity> */}

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.openWindow('rateApp')}>
//             <IconMC name="star-half-full" size={23} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Rate Us' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.openWindow('fb')}>
//             <IconFontAwesome5 name="facebook" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='FB Page' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.openWindow('fbgroup')}>
//             <IconFontAwesome5 name="facebook" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='FB Group' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.openWindow('youtube')}>
//             <IconMC name="youtube-tv" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Youtube' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.shareApp()}>
//             <IconFeather name="share-2" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Share this App' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => navigation.navigate('About')}>
//             <IconFontAwesome5 name="users" size={18} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='About Us' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => navigation.navigate('PrivacyPolicy')}>
//             <IconOcticons name="law" size={24} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Privacy Policy' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           {/* <TouchableOpacity 
//                   style={{marginTop:20,flexDirection:'row'}}
//                   onPress={() => navigation.navigate('MenuTab')}>
//                     <IconEntypo name="book" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//                     <RVText content='Book Store' style={{marginLeft: 15,fontSize: 16}} />
//                 </TouchableOpacity> */}

//           {/* <TouchableOpacity 
//                   style={{marginTop:20,flexDirection:'row'}}
//                   onPress={() => navigation.navigate('CopyRight')}>
//                     <IconAntDesign name="copyright" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//                     <RVText content='Copy Right' style={{marginLeft: 13,fontSize: 12,fontWeight:"bold"}} />
//                 </TouchableOpacity> */}

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.openWindow('web')}>
//             <IconMC name="web" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content='Web Portal' style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>

//           {/* <TouchableOpacity 
//                   style={{marginTop:20,flexDirection:'row'}}
//                   onPress={() => navigation.navigate('Notifications')}>
//                     <IconMC name="bell-ring-outline" size={23} color={COLOR.ICON_PRIMARY_COLOR} />
//                     <RVText content='Notifications' style={{marginLeft: 15,fontSize: 16}} />
//                 </TouchableOpacity> */}

//           <TouchableOpacity
//             style={{ marginTop: 20,marginLeft:10, flexDirection: 'row' }}
//             onPress={() => this.logoutHandler()}>
//             <IconAntDesign name="logout" size={18} color={COLOR.ICON_PRIMARY_COLOR} />
//             <RVText content={this.state.authText} style={{ marginLeft: 15, fontSize: 12, fontWeight: "bold" }} />
//           </TouchableOpacity>
//         </View>
//         <View style={{ borderTopWidth: 2, borderTopColor: COLOR.ICON_PRIMARY_COLOR, marginHorizontal: 10, marginVertical: 20 }}>
//           <View style={{ marginTop: 50, justifyContent: 'center', alignItems: 'center' }}>
//             <Text style={{ fontSize: 12 }}>App Version:1.0.0</Text>
//             <Text style={{ fontSize: 12 }}>Last Update:28 July,2021</Text>
//           </View>
//         </View>
//       </SafeAreaView>
//     )
//   }
// }
// const mapStateToProps = state => ({
//   auth: state.auth
// })
// export default connect(mapStateToProps, {
//   logout
// })(CustomDrawerContent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "grey",
    fontSize: 30,
    fontWeight: "bold"
  }
});