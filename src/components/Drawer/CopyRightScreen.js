import React,{ Component } from 'react';
import {  View, SafeAreaView,ActivityIndicator,StyleSheet,ScrollView,Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CustomHeader,CustomMessageBox } from '../MaterialDesign';

class CopyRightScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  async componentDidMount() {   

    let token = await AsyncStorage.getItem('token');
    if(token == null) {
      nextProps.navigation.navigate('Login');
    }
    this.setState({
      ...this.state,
      isLoading: false
    });
  }

  render() { 
    let { navigation } = this.props;
    let { isLoading } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader isHome={true} title='Copy Right' navigation = {navigation}/>
        <View style={{flex:1,backgroundColor: '#fff',marginBottom:20}}>
          {            
              isLoading == true ?
              <View style={{flex: 1,justifyContent: "center",alignItems: "center"}}>
                <ActivityIndicator size="large"></ActivityIndicator>
              </View>
              :
            <View style={{flex:1}}>
              {/* <View style={{height:30,justifyContent:"center",alignItems:"center",backgroundColor:'#000',opacity:0.9}}>
                <Text style={{color:'#fff',fontWeight:'bolder'}}>ABOUT US</Text>
              </View> */}
              <View style={{padding:15}}>                
                <ScrollView>
                  <Text style={styles.textStyle}>
                    CopyRightScreen ©my12class 2020 All Rights Reserved www.my12class.com
                  </Text>
                  <Text style={styles.textStyle}>
                    All files and information contained in this apps are CopyRightScreen by my12class, and may not be duplicated, copied, 
                    modified or adapted, in any way without our written permission. Our apps may contain our trademarks as well as those 
                    of our affiliates or other companies, in the form of words, graphics, and logos. Your use of our apps does not constitute 
                    any right or license for you to use our trademarks, without the prior written permission of my12class. Our Content, as found 
                    within our apps, is protected under ‘Bangladesh CopyRightScreen Act 2000’ and ‘Digital Millennium CopyRightScreen Act’. The copying, redistribution, 
                    use or publication by you of any such Content, is strictly prohibited. Your use of our apps does not grant you any ownership rights to our Content.  
                  </Text>
                </ScrollView>
              </View>
            </View>
          }
        </View>
        <CustomMessageBox/>      
      </SafeAreaView>
    )
  }
}
export default CopyRightScreen;

const styles = StyleSheet.create({
  textStyle: {
    lineHeight:25,
    marginBottom:15,
    textAlign:'justify'
  }
});