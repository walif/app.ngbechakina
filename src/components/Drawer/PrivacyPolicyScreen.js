import React, { Component } from 'react';
import { View, SafeAreaView, ActivityIndicator, StyleSheet, ScrollView, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CustomHeader, CustomMessageBox } from '../MaterialDesign';

class PrivacyPolicyScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  async componentDidMount() {

    let token = await AsyncStorage.getItem('token');
    if (token == null) {
      nextProps.navigation.navigate('Login');
    }
    this.setState({
      ...this.state,
      isLoading: false
    });
  }

  render() {
    let { navigation } = this.props;
    let { isLoading } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader isHome={true} title='Privacy Policy' navigation={navigation} />
        <View style={{ padding: 15,marginBottom:50 }}>
          <ScrollView showsVerticalScrollIndicator={false}>

            <Text style={styles.textStyle}>
              In order for the website to provide a safe and useful service, it is important for NGbechakena.com to collect, use, and share personal information.
            </Text>
            <Text style={styles.textStyle}>
              Collection{'\n'}
              Information posted on NGbechakena.com is publicly available. If you choose to provide us with personal information, you are consenting to the transfer and storage of that information on our servers. We collect and store the following personal information:
            </Text>
            <Text style={styles.textStyle}>
              Email address, contact information, and (depending on the service used) sometimes financial information
              Computer sign-on data, statistics on page views, traffic to and from NGbechakena.com and response to advertisements
              Other information, including users' IP address and standard web log information.
            </Text>
            <Text style={styles.textStyle}>
              Use{'\n'}
              We use users' personal information to:
            </Text>
            <Text style={styles.textStyle}>
              Provide our services{'\n'}
              Resolve disputes, collect fees, and troubleshoot problems
              Encourage safe trading and enforce our policies
              Customize users experience, measure interest in our services
              Improve our services and inform users about services and updates
              Communicate marketing and promotional offers to you according to your preferences
              Do other things for users as described when we collect the information{'\n'}
              Disclosure{'\n'}
              We don't sell or rent users' personal information to third parties for their marketing purposes without users' explicit consent. We may disclose personal information to respond to legal requirements, enforce our policies, respond to claims that a posting or other content violates other's rights, or protect anyone's rights, property, or safety.
            </Text>
            <Text style={styles.textStyle}>
              Communication and email tools{'\n'}
              You agree to receive marketing communications about consumer goods and services on behalf of our third party advertising partners unless you tell us that you prefer not to receive such communications. If you don't wish to receive marketing communications from us, simply indicate your preference by following directions provided with the communication. You may not use our site or communication tools to harvest addresses, send spam or otherwise breach our Terms of Use or Privacy Policy. We may automatically scan and manually filter email messages sent via our communication tools for malicious activity or prohibited content. If you use our tools to send content to a friend, we don't permanently store your friends' addresses or use or disclose them for marketing purposes. To report spam from other users, please contact customer support.
            </Text>
            <Text style={styles.textStyle}>
              Security{'\n'}
              We use lots of tools (encryption, passwords, physical security) to protect your personal information against unauthorized access and disclosure.

              All personal electronic details will be kept private by the Service except for those that you wish to disclose.
              It is unacceptable to disclose the contact information of others through the Service.
              If you violate the laws of your country of residence and/or the terms of use of the Service you forfeit your privacy rights over your personal information.
            </Text>

            <Text style={styles.textStyle}>
              Contact details{'\n'}
              Customer Support e-mail: support@ngbechakena.com{'\n\n'}

              Unsubscribe information{'\n'}
              If at any time you wish to have your information reviewed or removed from our active databases, please contact us at support@ngbechakena.com. Additionally, you will be able to unsubscribe anytime by clicking on the unsubscribe link at the bottom of all our email communications.{'\n\n'}

              This website makes use of Display Advertising, and uses Remarketing technology with Google Analytics to advertise online. Third-party vendors, including Google, may show our ads on various websites across the Internet, using first-party cookies and third-party cookies together to inform, optimize, and serve ads based on past visits to our website.{'\n\n'}

              Visitors can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads using the Ads Preferences Manager.
            </Text>

          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}
export default PrivacyPolicyScreen;

const styles = StyleSheet.create({
  textHeadLine: {
    fontWeight: 'bold'
  },
  textStyle: {
    lineHeight: 25,
    marginBottom: 15,
    textAlign: 'justify'
  }
});