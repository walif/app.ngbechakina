import React, { useState, useEffect } from 'react';
import {
    ActivityIndicator,
    View,
    StyleSheet,
    Image
} from 'react-native';
import { useDispatch } from 'react-redux';
import * as Types from './store/actions/types';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import FocusAwareStatusBar from './core/FocusAwareStatusBar';
import AuthHeaderToken from './utils/setAuthToken';
import { IMAGE } from './assets/image';


const SplashScreen = () => {

    const navigation = useNavigation();
    //State for ActivityIndicator animation
    const [animating, setAnimating] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            setAnimating(false);
            //Check if user_id is set or not
            //If not then send for Authentication
            //else send to Home Screen
            AsyncStorage.getItem('token').then((value) => {
                const user = JSON.parse(value);
                //console.log(user);
                let isAuthenticate = true;

                if (value === null)
                    isAuthenticate = false;
                else {
                    if (user.expiresIn < Date.now())
                        isAuthenticate = false
                };


                if (isAuthenticate) {
                    AuthHeaderToken(user.token);
                    dispatch({
                        type: Types.SET_USER,
                        payload: {
                            user: user
                        }
                    });
                    navigation.replace('HomeApp');
                }
                else {
                    // navigation.replace('Login');
                    navigation.replace('HomeApp');
                }
            });

        }, 2000);
    }, []);

    return (
        <View style={styles.container}>
            <FocusAwareStatusBar translucent backgroundColor='#eaeef7' />
            <Image
                source={IMAGE.APP_LOGO}
                style={{ width: '90%', resizeMode: 'contain', margin: 30 }}
            />
            <ActivityIndicator
                animating={animating}
                color="#307ecc"
                size="large"
                style={styles.activityIndicator}
            />
        </View>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    activityIndicator: {
        alignItems: 'center',
        height: 80,
    },
});