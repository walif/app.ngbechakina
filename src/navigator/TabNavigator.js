import React from 'react';
import { Alert } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconMC from 'react-native-vector-icons/MaterialCommunityIcons';
import { SITE,COLOR } from '../core';
import {
  HomeStack,
  BalanceStack,
  ProfileStack,
  MyAdsStack
} from './stack';
import { useSelector } from 'react-redux';

const Tab = createBottomTabNavigator();

export default TabNavigator = () => {

  const authState = useSelector(state => state.auth);

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? <IconFontAwesome5 name="home" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
              : <IconFontAwesome5 name="home" size={20} color="#000" />;
          } else if (route.name === 'MyAds') {
            iconName = focused
              ? <IconFontAwesome5 name="chalkboard-teacher" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
              : <IconFontAwesome5 name="chalkboard-teacher" size={20} color="#000" />;
          } else if (route.name === 'Profile') {
            iconName = focused
              ? <IconFontAwesome5 name="user-tie" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
              : <IconFontAwesome5 name="user-tie" size={20} color="#000" />;
          } else if (route.name === 'More') {
            iconName = focused
              ? <IconFontAwesome5 name="bars" size={20} color={COLOR.ICON_PRIMARY_COLOR} />
              : <IconFontAwesome5 name="bars" size={20} color="#000" />;
          }

          // You can return any component that you like here!
          return iconName;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }} >
      <Tab.Screen
        name="Home"
        component={HomeStack}
        listeners={({ navigation }) => ({
          tabPress: e => {
            // Prevent default action
            //e.preventDefault();
            //console.log(e);
            // Do something with the `navigation` object
            navigation.replace('Home');
          },
        })}
      />
      <Tab.Screen name="Profile"
        component={ProfileStack}
        listeners={({ navigation, route }) => ({
          tabPress: e => {
            // Prevent default action
            e.preventDefault();
            Alert.alert(
              'Profile',
              'This features release as soon as possible!',
              [
                {
                  text: 'Cancel',
                  style: 'cancel'
                },
                {
                  text: 'OK', onPress: () => {
                    // Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err));
                  }
                }
              ],
              { cancelable: false }
            );
            // Toast.show({
            //   text: "This feature release as soon as possible!",
            //   buttonText: "Ok",
            //   position: "bottom",
            //   textStyle: { fontSize: 14 },
            //   buttonTextStyle: {fontSize: 12}
            // });
          },
        })} />
      <Tab.Screen
        name="MyAds"
        component={MyAdsStack}
        options={{ title: 'My Ads' }}
        listeners={({ navigation, route }) => ({
          tabPress: e => {           
            

            if(!authState.user && !authState.isAuthenticated) {
              // console.log(authState);
              e.preventDefault();
              Alert.alert(
                'লগইন নোটিফিকেশন',
                'এই ফিচারটি পেতে লগইন করুন!',
                [
                  {
                    text: 'Cancel',
                    style: 'cancel'
                  },
                  {
                    text: 'OK', onPress: () => {
                      navigation.replace('Login');
                      // Linking.openURL(SITE.fbPage).catch((err) => console.error('An error occurred', err));
                    }
                  }
                ],
                { cancelable: false }
              );
            }
            else {
              navigation.navigate('MyAds', {
                screen: 'MyAds'
              });
            }

            

            //console.log(authState);
            // navigation.replace('MyAds');
            //console.log('Hello Tab!');
            // Prevent default action
            
            // Toast.show({
            //   text: "This feature release as soon as possible!",
            //   buttonText: "Ok",
            //   position: "bottom",
            //   textStyle: { fontSize: 14 },
            //   buttonTextStyle: {fontSize: 12}
            // });
          },
        })}
      />
      <Tab.Screen name="More"
        component={BalanceStack}
        options={{ title: 'More' }}
        listeners={({ navigation, route }) => ({
          tabPress: e => {
            // Prevent default action
            e.preventDefault();
           navigation.openDrawer(); 
          },
        })} />
    </Tab.Navigator>
  )
}