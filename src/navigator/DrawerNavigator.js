import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import TabNavigator from './TabNavigator';
import { 
    AboutScreen,
    CopyRightScreen,
    CustomDrawerContent,
    PrivacyPolicyScreen 
} from '../components';

const Drawer = createDrawerNavigator();

export default DrawerNavigator = () => {
  return (
    <Drawer.Navigator initialRouteName="MenuTab"
      drawerContent={props => <CustomDrawerContent {...props} />} >
      <Drawer.Screen name="MenuTab" component={TabNavigator} />
      <Drawer.Screen name="About" component={AboutScreen} />
      <Drawer.Screen name="PrivacyPolicy" component={PrivacyPolicyScreen} />
      <Drawer.Screen name="CopyRight" component={CopyRightScreen} />
    </Drawer.Navigator>
  )
}

