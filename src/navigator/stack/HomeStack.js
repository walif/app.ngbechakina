import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import navOptionHandler from '../NavigatorOptionHandler';
import { HomeController } from '../../components';

const StackHome = createStackNavigator();

export default HomeStack = () => {
  return (
    <StackHome.Navigator initialRouteName='Home'>
      <StackHome.Screen name="Home" component={HomeController} options={navOptionHandler} />
    </StackHome.Navigator>
  )
}