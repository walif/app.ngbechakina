import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import navOptionHandler from '../NavigatorOptionHandler';
import { MyAdsController } from '../../components/Tab/MyAds';

const StackProfile = createStackNavigator();

export default ProfileStack = () => {
  return (
    <StackProfile.Navigator initialRouteName='Profile'>
      <StackProfile.Screen name="Profile" component={MyAdsController} options={navOptionHandler} />
    </StackProfile.Navigator>
  )
}