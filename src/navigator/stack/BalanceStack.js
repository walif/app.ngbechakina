import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import navOptionHandler from '../NavigatorOptionHandler';
import AddBalanceScreen from '../../components/Tab/balance/AddBalanceScreen';

const StackBalance = createStackNavigator();

export default BalanceStack = () => {
  return (
    <StackBalance.Navigator initialRouteName='AddBalance'>
      <StackBalance.Screen name="AddBalance" component={AddBalanceScreen} options={navOptionHandler} />
    </StackBalance.Navigator>
  )
}