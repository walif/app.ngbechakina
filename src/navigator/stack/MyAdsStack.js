import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import navOptionHandler from '../NavigatorOptionHandler';
import { 
  MyAdsController,
  CategoryController,
  ProductController,
  CustomerProductListController,
  PendingProductListController,
  ApprovedProductListController,
  ProductDetailsController,
  ProductsViewController
} from '../../components';

const StackSetting = createStackNavigator();

export default MyAdsStack = () => {
  return (
    <StackSetting.Navigator initialRouteName='MyAds'>
      <StackSetting.Screen name="MyAds" component={MyAdsController} options={navOptionHandler} />
      <StackSetting.Screen name="Product" component={ProductController} options={navOptionHandler} />
      <StackSetting.Screen name="CustomerProductList" component={CustomerProductListController} options={navOptionHandler} />
      <StackSetting.Screen name="PendingProductList" component={PendingProductListController} options={navOptionHandler} />
      <StackSetting.Screen name="ApprovedProductList" component={ApprovedProductListController} options={navOptionHandler} />
      <StackSetting.Screen name="ProductDetails" component={ProductDetailsController} options={navOptionHandler} />
      <StackSetting.Screen name="ProductsView" component={ProductsViewController} options={navOptionHandler} />
    </StackSetting.Navigator>
  )
}