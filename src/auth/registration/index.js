import RegisterController from './RegisterController';
import RegisterScreen from './RegisterScreen';

export {
    RegisterController,
    RegisterScreen
}