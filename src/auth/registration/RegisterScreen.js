import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Label, Input, Button } from 'native-base';
import { RVText } from '../../core';
import { NetInfoSubscriptionController, AuthBackgroundTwo } from '../../components';

const RegisterScreen = ({ isNetCheck, name, mobile, email, password, confirmPassword,
  navigation,
  nameChangeHandler,
  mobileChangeHandler,
  // checkExistUser,
  passwordChangeHandler,
  confirmPasswordChangeHandler,
  emailChangeHandler,
  signupHandler }) => {

  return (
    <>
      {isNetCheck && <NetInfoSubscriptionController />}
      <AuthBackgroundTwo isLeftIcon={true} leftIconColor="#fff">
        {/* <Form> */}
        <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15, marginBottom: 50 }}>
          <View style={{ alignItems: 'center', paddingTop: 20 }}>
            <RVText content='New Account' style={{ fontWeight: 'bold', fontSize: 20 }} />
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Full Name</Label>
              <Input value={name}
                onChangeText={nameChangeHandler}
                style={{ fontSize: 12 }} />
            </Item>
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Mobile No.</Label>
              <Input value={mobile}
                onChangeText={mobileChangeHandler}
                // onBlur={checkExistUser}
                style={{ fontSize: 12 }}
                keyboardType='number-pad' />
            </Item>
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Email</Label>
              <Input value={email}
                onChangeText={emailChangeHandler}
                style={{ fontSize: 12 }}
                keyboardType='email-address' />
            </Item>
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Password</Label>
              <Input value={password}
                onChangeText={passwordChangeHandler}
                secureTextEntry={true}
                style={{ fontSize: 12 }} />
            </Item>
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Confirm Password</Label>
              <Input value={confirmPassword}
                onChangeText={confirmPasswordChangeHandler}
                secureTextEntry={true}
                style={{ fontSize: 12 }} />
            </Item>
          </View>

          <View>
            <Button block small rounded
              style={styles.authButton}
              onPress={signupHandler}>
              <RVText content={'Registration'} style={{ fontSize: 14, color: '#fff' }} />
            </Button>

            {/* <Button block small rounded
              style={[styles.authButton, { backgroundColor: '#00aaff' }]}
              onPress={() => navigation.navigate('Login')}>
              <RVText content={'Go Back'} style={{ fontSize: 14, color: '#fff' }} />
            </Button> */}
          </View>
        </View>
        {/* </Form> */}
      </AuthBackgroundTwo>
    </>
  )
}

export default RegisterScreen;

const styles = StyleSheet.create({
  authButton: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
    color: '#21216d'
  },
  input: {
    marginTop: 10
  }
})