import React, { Component } from 'react';
import { Toast } from 'native-base';
import { connect } from 'react-redux';
import { checkExistUser } from '../../store/actions/authAction';
import RegisterScreen from './RegisterScreen';
import { LoaderModalController } from '../../components';
import { signup } from '../../store/actions/authAction';
// import { ModalPopUpController } from '../../components/modal';

const init = {
  name: "",
  mobile: "",
  email: '',
  password: "",
  confirmPassword: "",
  isError: false,
  checkingMobile: false,
  duplicateMobile: false,
  showModal: false,
  imageType: 'loader',
  message: 'Loading...',
  isLoop: true,
  isSubmitClick: false,
}

class RegisterController extends Component {

  state = init
  // constructor(props) {
  //   super(props);
  //   this.state = 
  // }

  componentDidMount() {
    this.setState({
      ...this.state,
      // isLoading: false,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    //console.log('Outside Props:', this.props, 'Outside Prev Props:', prevProps);
    if (prevProps.auth.existUser && this.props.auth.existUser) {
      if (JSON.stringify(prevProps.auth.existUser) !== JSON.stringify(this.props.auth.existUser)) {
        //console.log('Insside Props:',this.props,'Inside Prev Props:',prevProps);

        //console.log(this.props.auth.existUser);

        const { isExist, message } = this.props.auth.existUser;

        if (isExist) {
          this.setState({
            ...this.state,
            isLoop: true,
            imageType: 'error',
            message: message
          });
          // this.setState({
          //   ...this.state,
          //   duplicateMobile: true,
          //   checkingMobile: false
          // }, () => {

          //   Toast.show({
          //     text: message,
          //     buttonText: "Ok",
          //     position: "bottom",
          //     textStyle: { fontSize: 14 },
          //     buttonTextStyle: { fontSize: 12 },
          //     style: {
          //       backgroundColor: "red"
          //     },
          //     duration: 3000
          //   });
          // })
        }
        // else { REDIRECT TO VERIFICATION PROCESS
        //   // this.setState({
        //   //   ...this.state,
        //   //   checkingMobile: false
        //   // });

        //   /**********************
        //     REDIRECT TO VERIFICATION PROCESS
        //   ***********************/
        //   let { name, mobile, email, password } = this.state;

        //   //DATA SENT FOR VERIFICATION       
        //   const user = {
        //     name: name,
        //     mobile: mobile,
        //     email: email,
        //     password: password,
        //     redirectTo: 'Login'
        //   }

        //   this.setState({
        //     ...this.state,
        //     name: "",
        //     mobile: "",
        //     email: '',
        //     password: "",
        //     confirmPassword: ""
        //   }, () => {
        //     // console.log(country, ":", user);
        //     // return;
        //     this.props.navigation.navigate('Verification', {
        //       user: user
        //     });

        //   });

        // }

        else {
          let { name, mobile, email, password } = this.state;
          this.setState({
            ...this.state,
            isSubmitClick: true
          }, () => {
            this.props.signup({ name, mobile, email, password });
          });
        }
      }
    }

    //GET ERROR
    if (this.state.isSubmitClick) {

      //console.log(this.props.auth);

      if (JSON.stringify(prevProps.auth.success) !== JSON.stringify(this.props.auth.success)) {
        //console.log(this.props.auth.error.message);
        this.setState({
          ...init,
          showModal: true,
          isSubmitClick: false,
          imageType: 'success',
          message: this.props.auth.success.message
        });
      }
      else if (JSON.stringify(prevProps.auth.error) !== JSON.stringify(this.props.auth.error)) {
        //console.log(this.props.auth.error.message);
        this.setState({
          ...this.state,
          showModal: true,
          isSubmitClick: false,
          imageType: 'error',
          message: this.props.auth.error.message
        });
      }
    }
  }

  nameChangeHandler = (val) => {
    this.setState({
      ...this.state,
      name: val
    });
  }

  mobileChangeHandler = (val) => {
    this.setState({
      ...this.state,
      mobile: val
    });
  }

  // checkExistUser = () => {
  //   this.setState({
  //     ...this.state,
  //     checkingMobile: true
  //     // isLoading: true
  //   }, () => {
  //     //console.log(this.state);
  //     this.props.checkExistUser(this.state.mobile);
  //   });
  // }

  emailChangeHandler = (val) => {
    this.setState({
      ...this.state,
      email: val
    });
  }

  passwordChangeHandler = (val) => {
    this.setState({
      ...this.state,
      password: val
    });
  }

  confirmPasswordChangeHandler = (val) => {
    this.setState({
      ...this.state,
      confirmPassword: val
    });
  }

  validateData = () => {
    let { mobile, password, confirmPassword, email, duplicateMobile } = this.state;

    const result = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let error = false;
    let message = "";
    if (mobile === "") {
      error = true;
      message = 'Please enter your mobile no.';
    }
    else if (password === "") {
      error = true;
      message = 'Please enter your password.';
    }
    else if (password.length < 4) {
      error = true;
      message = 'Password will be at least 4 charecters.';
    }
    else if (password !== confirmPassword) {
      error = true;
      message = 'Password don\'t match.';
    }
    else if (email) {
      if (!result.test(email)) {
        error = true;
        message = 'Invalid email address.';
      }
    }
    // else if (checkingMobile) {
    //   error = true;
    //   message = "System checking mobile no.Please wait a second.";
    // }
    // else if (duplicateMobile) {
    //   error = true;
    //   message = "This mobile no. is already exist in our system.Try another mobile no.";
    // }

    if (error) {
      Toast.show({
        text: message,
        buttonText: "Ok",
        position: "bottom",
        textStyle: { fontSize: 14 },
        buttonTextStyle: { fontSize: 12 },
        style: {
          backgroundColor: "red"
        },
        duration: 3000
      });
      return error;
    }
    return error;
  }

  signupHandler = () => {

    if (this.validateData()) {
      return;
    }

    this.setState({
      ...this.state,
      showModal: true,
      imageType: 'loader',
      message: 'Loading...'
    }, () => {
      this.props.checkExistUser(this.state.mobile);
    });



    // console.log(this.state);
    // return;

    // this.setState({
    //   ...this.init
    // }, () => {

    //   let { name, mobile, email, password } = this.state;
    //   // this.props.signup({ name, mobile, email, password });
    //   // return;
    //   //DATA SENT FOR VERIFICATION       
    //   const user = {
    //     name: name,
    //     mobile: mobile,
    //     email: email,
    //     password: password,
    //     redirectTo: 'Login'
    //   }

    //   // console.log(country, ":", user);
    //   // return;

    //   this.props.navigation.navigate('Verification', {
    //     user: user
    //   });

    // });
    //this.props.signup({ name,mobile,email,password });
  }

  closeModal = (returnValue) => {
    //console.log(returnValue);
    this.setState({
      ...this.state,
      showModal: false
    });
  }

  render() {
    let { navigation } = this.props;
    // const { name, mobile,password,confirmPassword,showModal,message,imageType} = this.state;
    const { name, mobile, email, password, confirmPassword, showModal, imageType, message, isLoop } = this.state;

    //console.log(this.state);
    return (
      <>

        <LoaderModalController
          visible={showModal}
          imageType={imageType}
          content={message}
          isLoop={isLoop}
          closeModal={this.closeModal} />

        <RegisterScreen isNetCheck={true}
          name={name}
          mobile={mobile}
          email={email}
          password={password}
          confirmPassword={confirmPassword}
          navigation={navigation}
          nameChangeHandler={this.nameChangeHandler}
          mobileChangeHandler={this.mobileChangeHandler}
          // checkExistUser={this.checkExistUser}
          passwordChangeHandler={this.passwordChangeHandler}
          confirmPasswordChangeHandler={this.confirmPasswordChangeHandler}
          emailChangeHandler={this.emailChangeHandler}
          signupHandler={this.signupHandler}
        />

      </>
    )
  }
}
const mapStateToProps = state => ({
  auth: state.auth
})
export default connect(mapStateToProps, { checkExistUser, signup })(RegisterController);
