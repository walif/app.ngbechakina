import React, { Component } from 'react';
import { Toast } from 'native-base';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LoaderModalController } from '../../components';
import { login } from '../../store/actions/authAction';
import LoginScreen from './LoginScreen';


const init = {
  userName: null,
  password: null,
  showModal: false,
  imageType: 'loader',
  message: 'Loading...',
  isLoop: true,
  isLoggedinClick: false,
};

class LoginController extends Component {

  state = init;

  componentDidMount() {

    try {
      AsyncStorage.getItem('token').then((value) => {
        const user = JSON.parse(value);
        let isAuthenticate = true;

        if (value === null)
          isAuthenticate = false;
        else {
          if (user.expiresIn < Date.now())
            isAuthenticate = false
        };

        this.setState({
          ...this.state,
          showModal: false
        }, () => {
          if (isAuthenticate) {
            this.props.navigation.navigate('HomeApp');
          }
        })
      });
    }
    catch (ex) {
      this.setState({
        ...this.state,
        showModal: true,
        message: ex,
        isLoop: true,
        imageType: 'error',
      }
      );
    }
  }

  // async UNSAFE_componentWillReceiveProps(nextProps) {
  //   //console.log('Login Next Prob:',nextProps);
  //   //LOGGEDIN FAILED
  //   if(this.state.isLoggedinClick) {
  //     let token = await AsyncStorage.getItem('token');
  //     if(token) {
  //       let user = JSON.parse(token);
  //       this.setState({
  //         ...this.state,
  //         isLoading: false,
  //         isLoggedinClick: false
  //       });
  //       nextProps.navigation.navigate('HomeApp');
  //     }
  //     else {
  //       this.setState({
  //         ...this.state,
  //         isLoading: false,
  //         isLoggedinClick: false
  //       });

  //       Alert.alert(
  //         'Login Failed!',
  //         'Your mobile no. or password is invalid!',
  //         [          
  //           { text: 'OK'/*,onPress: ()=> this.myCustomFunction() nextProps.navigation.navigate('Login')*/ }
  //         ],
  //         { cancelable: false }
  //       );
  //     }
  //   }
  // if(nextProps.auth) {      
  //   //console.log(this.state,nextProps); 
  //   if(nextProps.auth.isLoading == false) {
  //     this.state.isLoading = false;
  //   }
  //   if(nextProps.auth.isAuthenticated) {
  //     //console.log(nextProps);
  //     nextProps.navigation.navigate('HomeApp');
  //   }
  //   if(nextProps.auth.user) {

  //     if(nextProps.auth.isAuthenticated == false) {
  //       Alert.alert(
  //         'Login Failed!',
  //         'Your mobile no. or password is invalid!',
  //         [          
  //           { text: 'OK',onPress: ()=> this.myCustomFunction()/*nextProps.navigation.navigate('Login')*/ }
  //         ],
  //         { cancelable: false }
  //       );
  //     }        
  //   }
  // }
  //console.log(this.state,nextProps); 
  // }

  componentDidUpdate(prevProps, prevState) {

    //console.log(this.props.auth);

    if (this.state.isLoggedinClick) {
      if (this.props.auth.isAuthenticated) {
        this.setState({
          ...this.state,
          showModal: false,
          isLoggedinClick: false
        }, () => {
          // this.props.navigation.closeDrawer();
          this.props.navigation.navigate('HomeApp');
        })
      }
      else if (JSON.stringify(prevProps.auth.error) !== JSON.stringify(this.props.auth.error)) {
        setTimeout(() => this.setState({
          ...this.state,
          // showModal: true,
          message: this.props.auth.error.message,
          imageType: 'error',
          isLoop: true,
          isLoggedinClick: false
        }), 200)
      }
    }
  }

  userNameChangeHandler = (val) => {
    this.setState({
      ...this.state,
      userName: val
    });
  }
  passwordChangeHandler = (val) => {
    this.setState({
      ...this.state,
      password: val
    });
  }

  validateData = () => {
    //console.log(this.state);
    let { userName, password } = this.state;
    let error = false;
    let message = "";
    if (userName === "" || userName === null) {
      error = true;
      message = 'Please enter your mobile no.';
    }
    else if (password === "" || password === null) {
      error = true;
      message = 'Please enter your password.';
    }

    if (error) {
      Toast.show({
        text: message,
        buttonText: "Ok",
        position: "bottom",
        textStyle: { fontSize: 14 },
        buttonTextStyle: { fontSize: 12 },
        style: {
          backgroundColor: "red"
        },
        duration: 3000
      });
      return error;
    }
    return error;
  }

  loginHandler = () => {

    if (this.validateData())
      return;

    try {
      this.setState({
        ...this.state,
        showModal: true,
        message: 'Loading...',
        imageType: 'loader',
        isLoop: true,
        isLoggedinClick: true
      }, () => {
        let { userName, password } = this.state;
        this.props.login({ userName, password });
      });
    }
    catch (ex) {
      this.setState({
        ...this.state,
        showModal: true,
        message: ex,
        imageType: 'error',
        isLoggedinClick: false
      }
        // , () => {
        //   Toast.show({
        //     text: ex,
        //     buttonText: "Ok",
        //     position: "bottom",
        //     textStyle: { fontSize: 14 },
        //     buttonTextStyle: { fontSize: 12 },
        //     style: {
        //       backgroundColor: "red"
        //     },
        //     duration: 3000
        //   });
        // }
      );
    }
  }

  closeModal = (returnValue) => {
    //console.log(returnValue);
    this.setState({
      ...this.state,
      showModal: false
    });
  }

  render() {
    let { navigation } = this.props;
    const { userName, password, showModal, imageType, message, isLoop } = this.state;
    return (
      <>
        <LoaderModalController
          visible={showModal}
          imageType={imageType}
          content={message}
          isLoop={isLoop}
          closeModal={this.closeModal} />

        <LoginScreen
          userName={userName}
          password={password}
          isNetCheck={true}
          navigation={navigation}
          userNameChangeHandler={this.userNameChangeHandler}
          passwordChangeHandler={this.passwordChangeHandler}
          loginHandler={this.loginHandler}
        />
      </>
    )
  }
}
const mapStateToProps = state => ({
  auth: state.auth
})
export default connect(mapStateToProps, { login })(LoginController);
