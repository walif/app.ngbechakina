import React from 'react';
import {
  View, TouchableOpacity, StyleSheet, Dimensions
} from 'react-native';
import { Item, Label, Input, Button } from 'native-base';
import { RVText } from '../../core';

import { NetInfoSubscriptionController, AuthBackgroundTwo } from '../../components';

const LoginScreen = ({
  userName,
  password,
  isNetCheck,
  navigation,
  userNameChangeHandler,
  passwordChangeHandler,
  loginHandler }) => {

  return (
    <>
      {isNetCheck && <NetInfoSubscriptionController />}
      <AuthBackgroundTwo>
        <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 15, paddingRight: 15 }}>
          <View style={{ alignItems: 'center', paddingTop: 40, paddingBottom: 20 }}>
            <RVText content='Login Account' style={{ fontWeight: 'bold', fontSize: 20 }} />
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Mobile No.</Label>
              <Input onChangeText={userNameChangeHandler}
                value={userName}
                style={{ fontSize: 12 }}
                keyboardType='number-pad' />
            </Item>
          </View>

          <View style={styles.input}>
            <Item floatingLabel>
              <Label style={{ fontSize: 14 }}>Password</Label>
              <Input secureTextEntry={true}
                onChangeText={passwordChangeHandler}
                value={password}
                style={{ fontSize: 12 }} />
            </Item>
          </View>

          <Button block small rounded style={styles.authButton} onPress={loginHandler}>
            <RVText content={'Login'} style={{ fontSize: 14, color: '#fff' }} />
          </Button>
          <Button block small rounded style={[styles.authButton, { backgroundColor: '#00aaff' }]} onPress={() => navigation.navigate('Register')}>
            <RVText content={'Registration'} style={{ fontSize: 14, color: '#fff' }} />
          </Button>
          <View style={{ alignItems: 'center', marginTop: 5, marginBottom: 50 }}>
            <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
              <RVText content='Forgot Password? Recovery Now.' style={{ fontSize: 12, fontWeight: 'normal', color: '#FF0000' }} />
            </TouchableOpacity>
          </View>
        </View>
      </AuthBackgroundTwo>
    </>
  )
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  // rest of the styles
  svgCurve: {
    position: 'absolute',
    width: Dimensions.get('window').width
  },
  authButton: {
    marginTop: 20,
    justifyContent: "center",
    color: '#21216d',
    flexWrap: 'wrap'
  },
  contentContainer: {
    flex: 3,
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: 30,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 40,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  input: {
    marginTop: 10
  }
});