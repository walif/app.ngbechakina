import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Button, Input, Label } from 'native-base';
import { NetInfoSubscriptionController, AuthBackgroundTwo } from '../../components';
import { RVText } from '../../core';

const ForgotPasswordScreen = ({ userName, isNetCheck, navigation, userNameChangeHandler, submitHandler }) => {

    return (
        <>
            { isNetCheck && <NetInfoSubscriptionController />}
            <AuthBackgroundTwo>
                {/* <Form> */}
                    <View style={{ flex:1,paddingLeft: 15, paddingRight: 15,marginBottom:50 }}>
                        <View style={{ alignItems: 'center', paddingTop: 40, paddingBottom: 20 }}>
                            <RVText content='Recovery Account' style={{ fontWeight: 'bold', fontSize: 20 }} />
                        </View>
                        <Item floatingLabel>
                            <Label style={{ fontSize: 14 }}>Enter Your Mobile No.</Label>
                            <Input onChangeText={userNameChangeHandler}
                                value={userName}
                                style={{ fontSize: 12 }}
                                keyboardType='number-pad' />
                        </Item>
                        <Button block small rounded
                            style={styles.authButton}
                            onPress={submitHandler}>
                            <RVText content={'SEND OTP'} style={{ fontSize: 14, color: '#fff' }} />
                        </Button>
                        <Button block small rounded
                            style={[styles.authButton, { backgroundColor: '#00aaff' }]}
                            onPress={() => navigation.navigate('Login')}>
                            <RVText content={'Go Back'} style={{ fontSize: 14, color: '#fff' }} />
                        </Button>
                    </View>
                {/* </Form> */}

            </AuthBackgroundTwo>
        </>
    )
}

export default ForgotPasswordScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    authButton: {
        marginTop: 20,
        justifyContent: "center",
        color: '#21216d',
        flexWrap: 'wrap'
    }
})