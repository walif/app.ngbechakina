import React, { Component } from 'react';
import { Toast } from 'native-base';
import { connect } from 'react-redux';
import { checkExistUser } from '../../store/actions/authAction';
import { LoaderModalController } from '../../components';
import ForgotPasswordScreen from './ForgotPasswordScreen';

const init = {
    mobile: "",
    showModal: false,
    imageType: null,
    message: '',
    isLoop: false
}

class ForgotPasswordController extends Component {

    state = init;

    //   componentDidMount() {
    //     this.setState({
    //       ...this.state,
    //     });
    //   }

    componentDidUpdate(prevProps, prevState) {
        //console.log('Outside Props:', this.props, 'Outside Prev Props:', prevProps);
        if (JSON.stringify(prevProps.auth.existUser) !== JSON.stringify(this.props.auth.existUser)) {
            //console.log('Insside Props:',this.props,'Inside Prev Props:',prevProps);

            //console.log(this.props.auth.existUser);

            const { isExist, errors, statusCode } = this.props.auth.existUser;

            if (isExist === false) {
                this.setState({
                    ...this.state,
                    showModal: true,
                    imageType: 'error',
                    isLoop: false,
                    message: errors.toString()
                });
                // this.setState({
                //     ...this.state,
                //     duplicateMobile: true,
                //     checkingMobile: false
                // }, () => {
                //     Toast.show({
                //         text: errors.toString(),
                //         buttonText: "Ok",
                //         position: "bottom",
                //         textStyle: { fontSize: 14 },
                //         buttonTextStyle: { fontSize: 12 },
                //         style: {
                //             backgroundColor: "red"
                //         },
                //         duration: 3000
                //     });
                // })
            }
            else if (isExist) {
                const user = {
                    mobile: this.state.mobile,
                    redirectTo: 'ConfirmPassword'
                }
                this.props.navigation.navigate('Verification', {
                    user: user
                });
            }
        }
    }

    mobileChangeHandler = (val) => {
        this.setState({
            ...this.state,
            mobile: val
        });
    }

    validateData = () => {
        let { mobile } = this.state;
        let error = false;
        let message = "";
        if (mobile === "") {
            error = true;
            message = 'Please enter your mobile no.';
        }

        if (error) {
            Toast.show({
                text: message,
                buttonText: "Ok",
                position: "bottom",
                textStyle: { fontSize: 14 },
                buttonTextStyle: { fontSize: 12 },
                style: {
                    backgroundColor: "red"
                },
                duration: 3000
            });
            return error;
        }
        return error;
    }

    submitHandler = () => {

        if (this.validateData()) {
            return;
        }
        this.setState({
            ...this.state,
            showModal: true,
            imageType: 'loader',
            message: 'Loading...',
            isLoop: true
        }, () => {
            this.props.checkExistUser(this.state.mobile);
        })

        // console.log(this.state);
        // return;

        // this.setState({
        //     ...this.init
        // }, () => {

        //     let { mobile } = this.state;
        //     // this.props.signup({ name, mobile, email, password });
        //     // return;
        //     //DATA SENT FOR VERIFICATION       
        //     const user = {
        //         mobile: mobile,
        //         redirectTo: 'ConfirmPassword'
        //     }
        //     this.props.navigation.navigate('Verification', {
        //         user: user
        //     });

        // });
    }

    closeModal = (returnValue) => {
        //const { imageType, url } = returnValue;
        this.setState({
            ...this.state,
            showModal: false
        });
    }

    render() {
        let { navigation } = this.props;
        const { mobile, showModal, imageType, message, isLoop } = this.state;
        return (
            <>
                <LoaderModalController
                    visible={showModal}
                    imageType={imageType}
                    content={message}
                    isLoop={isLoop}
                    closeModal={this.closeModal} />

                <ForgotPasswordScreen isNetCheck={true}
                    userName={mobile}
                    navigation={navigation}
                    userNameChangeHandler={this.mobileChangeHandler}
                    submitHandler={this.submitHandler}
                />
            </>
        )
    }
}
const mapStateToProps = state => ({
    auth: state.auth
})
export default connect(mapStateToProps, { checkExistUser })(ForgotPasswordController);
