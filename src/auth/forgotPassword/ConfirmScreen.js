import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Button, Input, Label } from 'native-base';
import { RVText } from '../../core';
import { NetInfoSubscriptionController, AuthBackgroundTwo } from '../../components';

const ConfirmScreen = ({ password, confirmPassword, isNetCheck, navigation,
    passwordChangeHandler,
    confirmPasswordChangeHandler,
    submitHandler
}) => {

    return (
        <>
            { isNetCheck && <NetInfoSubscriptionController />}
            <AuthBackgroundTwo>
                {/* <Form> */}
                <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15,marginBottom:50 }}>
                    <View style={{ alignItems: 'center', paddingTop: 40, paddingBottom: 20 }}>
                        <RVText content='Recovery Account' style={{ fontWeight: 'bold', fontSize: 20 }} />
                    </View>
                    <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>Password</Label>
                        <Input value={password}
                            onChangeText={passwordChangeHandler}
                            secureTextEntry={true}
                            style={{ fontSize: 12 }} />
                    </Item>
                    <Item floatingLabel>
                        <Label style={{ fontSize: 14 }}>Confirm Password</Label>
                        <Input value={confirmPassword}
                            onChangeText={confirmPasswordChangeHandler}
                            secureTextEntry={true}
                            style={{ fontSize: 12 }} />
                    </Item>

                    <Button block small rounded
                        style={styles.authButton}
                        onPress={submitHandler}>
                        <RVText content={'Change Password'} style={{ fontSize: 14, color: '#fff' }} />
                    </Button>
                    {/* <Button block small rounded
                        style={[styles.authButton, { backgroundColor: '#00aaff' }]}
                        onPress={() => navigation.navigate('ForgotPassword')}>
                        <RVText content={'Go Back'} style={{ fontSize: 14, color: '#fff' }} />
                    </Button> */}
                </View>
                {/* </Form> */}
            </AuthBackgroundTwo>
        </>
    )
}

export default ConfirmScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    authButton: {
        marginTop: 20,
        justifyContent: "center",
        color: '#21216d',
        flexWrap: 'wrap'
    }
})