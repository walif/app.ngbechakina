import React, { Component } from 'react';
import { Toast } from 'native-base';
import { connect } from 'react-redux';
import { recoverAccount } from '../../store/actions/authAction';
import { LoaderModalController } from '../../components';
import ConfirmScreen from './ConfirmScreen';

const init = {
  mobile: "",
  password: "",
  confirmPassword: "",
  //isSubmitClick: false,
  showModal: false,
  imageType: 'loader',
  message: '',
  isLoop: false,
}

class ConfirmController extends Component {

  state = init;

  componentDidMount() {
    console.log(this.props);
    this.setState({
      ...this.state,
      mobile: this.props.auth.existUser.data
    });

    //console.log(this.props.auth);
    // this.setState({
    //   ...this.state,
    //   // isLoading: false,
    // });
  }

  componentDidUpdate(prevProps, prevState) {
    const { error,success } = this.props.auth;
    if (JSON.stringify(prevProps.auth.error) !== JSON.stringify(error)) {
      this.setState({
        ...this.state,
        showModal: true,
        imageType: 'error',
        isLoop: false,
        message: error.message.toString()
      });
    }
    else if(JSON.stringify(prevProps.auth.success) !== JSON.stringify(success)) {
      this.setState({
        ...this.state,
        showModal: true,
        imageType: 'success',
        isLoop: false,
        message: success.message.toString()
      });
    }
    // //console.log('Outside Props:', this.props, 'Outside Prev Props:', prevProps);
    // if (JSON.stringify(prevProps.auth.existUser) !== JSON.stringify(this.props.auth.existUser)) {
    //   //console.log('Insside Props:',this.props,'Inside Prev Props:',prevProps);

    //   //console.log(this.props.auth.existUser);

    //   const {isExist,message} = this.props.auth.existUser;

    //   if(isExist) {
    //     this.setState({
    //           ...this.state,
    //           duplicateMobile: true,
    //           checkingMobile: false
    //         }, () => {
    //           Toast.show({
    //             text: message,
    //             buttonText: "Ok",
    //             position: "bottom",
    //             textStyle: { fontSize: 14 },
    //             buttonTextStyle: { fontSize: 12 },
    //             style: {
    //               backgroundColor: "red"
    //             },
    //             duration: 3000
    //           });
    //         })
    //   }
    //   else {
    //     this.setState({
    //       ...this.state,
    //       checkingMobile: false
    //     });
    //   }
    // }
  }

  passwordChangeHandler = (val) => {
    this.setState({
      ...this.state,
      password: val
    });
  }
  confirmPasswordChangeHandler = (val) => {
    this.setState({
      ...this.state,
      confirmPassword: val
    });
  }

  validateData = () => {
    let { mobile, password, confirmPassword } = this.state;
    let error = false;
    let message = "";
    // if (mobile === "") {
    //   error = true;
    //   message = 'Please enter your mobile no.';
    // }
    // else 
    if (password === "") {
      error = true;
      message = 'Please enter your password.';
    }
    else if (password.length < 4) {
      error = true;
      message = 'Password will be at least 4 charecters.';
    }
    else if (password !== confirmPassword) {
      error = true;
      message = 'Password don\'t match.';
    }
    // else if (checkingMobile) {
    //   error = true;
    //   message = "System checking mobile no.Please wait a second.";
    // }
    // else if (duplicateMobile) {
    //   error = true;
    //   message = "This mobile no. is already exist in our system.Try another mobile no.";
    // }

    if (error) {
      Toast.show({
        text: message,
        buttonText: "Ok",
        position: "bottom",
        textStyle: { fontSize: 14 },
        buttonTextStyle: { fontSize: 12 },
        style: {
          backgroundColor: "red"
        },
        duration: 3000
      });
      return error;
    }
    return error;
  }

  submitHandler = () => {

    if (this.validateData()) {
      return;
    }

    this.setState({
      ...this.state,
      showModal: true,
      imageType: 'loader',
      message: 'Loading...',
      isLoop: true,
    }, () => {
      const { mobile, password } = this.state;
      this.props.recoverAccount({ mobile, password });
    });

    // console.log(this.state);
    // return;

    // this.setState({
    //   ...this.init
    // }, () => {

    //   let { name, mobile, email, password } = this.state;
    //   // this.props.signup({ name, mobile, email, password });
    //   // return;
    //   //DATA SENT FOR VERIFICATION       
    //   const user = {
    //     name: name,
    //     mobile: mobile,
    //     email: email,
    //     password: password
    //   }
    //   this.props.navigation.navigate('Verification', {
    //     user: user
    //   });

    // });
    //this.props.signup({ name,mobile,email,password });
  }

  closeModal = (returnValue) => {
    const { imageType, url } = returnValue;
    this.setState({
      ...this.state,
      showModal: false
    }, () => {
      return imageType === 'success' ? this.props.navigation.replace(url) : null;
    });
  }

  render() {
    let { navigation } = this.props;
    // const { name, mobile,password,confirmPassword,showModal,message,imageType} = this.state;
    const { password, confirmPassword, showModal, imageType, message, isLoop } = this.state;

    //console.log(this.state);
    return (
      <>

        <LoaderModalController
          visible={showModal}
          imageType={imageType}
          content={message}
          isLoop={isLoop}
          redirectTo={'Login'}
          closeModal={this.closeModal} />

        <ConfirmScreen isNetCheck={true}
          password={password}
          confirmPassword={confirmPassword}
          navigation={navigation}
          passwordChangeHandler={this.passwordChangeHandler}
          confirmPasswordChangeHandler={this.confirmPasswordChangeHandler}
          submitHandler={this.submitHandler}
        />

      </>
    )
  }
}
const mapStateToProps = state => ({
  auth: state.auth
})
export default connect(mapStateToProps, { recoverAccount })(ConfirmController);
