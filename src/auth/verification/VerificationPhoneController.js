import React, { Component, useState } from 'react';
// import { Alert } from 'react-native';
import { Toast } from 'native-base';
import auth from '@react-native-firebase/auth';
import VerificationPhoneScreen from './VerificationPhoneScreen';
import { connect } from 'react-redux';
import { signup } from '../../store/actions/authAction';
import { LoaderModalController, NetInfoSubscriptionController } from '../../components';
import * as RNLocalize from "react-native-localize";
import Countries from '../../assets/CountryCodes.json';

const init = {
  // isLoading: true,
  code: '',
  confirmCode: '',
  isSubmitClick: false,
  showModal: false,
  imageType: 'loader',
  message: '',
  redirectTo: ''
}

class VerificationPhoneController extends Component {

  state = init

  componentDidMount() {

    this.signUpWithPhoneNumber();

    // this.setState({
    //   ...this.state,
    //   showModal: true,
    //   message: 'Loading...',
    //   imageType: 'loader',
    //   isLoop: true
    // }, () => {
    //   this.signUpWithPhoneNumber();
    // })
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.state.isSubmitClick) {
      //console.log(this.props.auth);

      if (JSON.stringify(prevProps.auth.success) !== JSON.stringify(this.props.auth.success)) {
        //console.log(this.props.auth.error.message);
        this.setState({
          ...init,
          showModal: true,
          isSubmitClick: false,
          imageType: 'success',
          message: this.props.auth.success.message,
          isLoop: false,
          redirectTo: this.state.redirectTo
        });
      }
      else if (JSON.stringify(prevProps.auth.error) !== JSON.stringify(this.props.auth.error)) {
        //console.log(this.props.auth.error.message);
        this.setState({
          ...this.state,
          showModal: true,
          isSubmitClick: false,
          imageType: 'error',
          isLoop: false,
          message: this.props.auth.error.message
        });
      }
    }
  }

  signUpWithPhoneNumber = () => {

    try {

      //GET COUNTRY CODE
      let country = Countries.find(c => c.code.toLocaleLowerCase() === RNLocalize.getCountry().toLocaleLowerCase());

      const { mobile, redirectTo } = this.props.route.params.user;
      const phoneNumber = country.dial_code + mobile;
      //console.log(phoneNumber);

      // const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
      // //console.log(confirmation);

      this.setState({
        ...this.state,
        showModal: true,
        message: 'Loading...',
        imageType: 'loader',
        isLoop: true,
        redirectTo: redirectTo
      }, () => {
        
        auth().signInWithPhoneNumber(phoneNumber)
          .then((confirmation) => {

            this.setState({
              ...this.state,
              showModal: false,
              message: 'get sms code',
              imageType: 'success',
              isLoop: false,
              confirmCode: confirmation
            });

          })
          .catch((error) => {
            const { code, message } = error;
            this.setState({
              ...this.state,
              // showModal: true,
              imageType: 'error',
              isLoop: false,
              message: message
            });
          });

      });
    }
    catch (ex) {
      
      if(this.state.showModal === true) {
        this.setState({
          ...this.state,
          // showModal: true,
          message: JSON.stringify(ex),
          imageType: 'error',
          isLoop: false
        });
      }
      else {
        this.setState({
          ...this.state,
          showModal: true,
          message: JSON.stringify(ex),
          imageType: 'error',
          isLoop: false
        });
      }

      // if (ex.code === 'auth/invalid-phone-number') {
      //   this.setState({
      //     ...this.state,
      //     showModal: ,
      //     message: 'Your mobile no. is invalid.Please provide valid mobile no.',
      //     imageType: 'error',
      //     isLoop: false
      //   });
      // }
      // else {
      //   this.setState({
      //     ...this.state,
      //     showModal: true,
      //     message: JSON.stringify(ex),
      //     imageType: 'error',
      //     isLoop: false
      //   });
      // }

      // this.setState({
      //   ...this.state,
      //   showModal: false
      // }
      // , () => {
      //   if (ex.code === 'auth/invalid-phone-number') {
      //     Toast.show({
      //       text: "Your mobile no. is invalid.Please provide valid mobile no!",
      //       buttonText: "Ok",
      //       position: "bottom",
      //       textStyle: { fontSize: 14 },
      //       buttonTextStyle: { fontSize: 12 },
      //       style: {
      //         backgroundColor: "red"
      //       },
      //       duration: 3000
      //     });
      //   }
      // }
      // )
    }
  }

  setCodeChangeHandler = (value) => {
    //console.log(e);
    this.setState({
      ...this.state,
      code: value
    })
  }

  validateData = () => {
    let error = false;
    if (this.state.code === "" || !this.state.confirmCode) {
      error = true;
      Toast.show({
        text: "Verification code is required!",
        buttonText: "Ok",
        position: "bottom",
        textStyle: { fontSize: 14 },
        buttonTextStyle: { fontSize: 12 },
        style: {
          backgroundColor: "red"
        },
        duration: 3000
      });
    }
    return error;
  }

  confirmCodeHandler = async () => {
    if (this.validateData())
      return;

    try {
      this.setState({
        ...this.state,
        showModal: true,
        imageType: 'loader',
        message: 'Loading...',
        isLoop: true,
        isSubmitClick: true,
      }, async () => {
        var result = await this.state.confirmCode.confirm(this.state.code);
        //console.log(result);

        //console.log(this.state);

        if (result.user) { //REGISTRATION          

          if (this.state.redirectTo === 'Login') {
            let { name, mobile, email, password } = this.props.route.params.user;
            this.props.signup({ name, mobile, email, password });
          }
          else if (this.state.redirectTo === 'ConfirmPassword')
            this.setState({
              ...this.state,
              showModal: false
            }, () => {
              this.props.navigation.replace(this.state.redirectTo);
            })
        }
      })
    }
    catch (ex) {
      this.setState({
        ...this.state,
        imageType: 'error',
        message: ex,
        isLoop: false,
        isSubmitClick: false,
      }
        // , () => {
        //   Toast.show({
        //     text: 'Invalid code.',
        //     buttonText: "Ok",
        //     position: "bottom",
        //     textStyle: { fontSize: 14 },
        //     buttonTextStyle: { fontSize: 12 },
        //     style: {
        //       backgroundColor: "red"
        //     },
        //     duration: 3000
        //   });
        // }
      )
    }
  }

  resendCodeHandler = () => {
    this.signUpWithPhoneNumber();
    //console.log(this.state);
    // this.setState({
    //   ...this.state,
    //   showModal: true,
    //   imageType: 'loader',
    //   message: 'Loading...',
    //   isLoop: true,
    // }, () => {
    //   this.signUpWithPhoneNumber()
    // })
  }

  closeModal = (returnValue) => {
    const { imageType, url } = returnValue;
    //console.log('VP:',returnValue);

    this.setState({
      ...this.state,
      showModal: false
    }, () => {
      return imageType === 'success' ? this.props.navigation.replace(url) : null;
    });

  }


  render() {
    const { code, showModal, imageType, message, isLoop, redirectTo } = this.state;
    return (
      <>
        {/* CHECK INTERNET */}
        <NetInfoSubscriptionController />

        {/* MODAL MESSAGE */}
        <LoaderModalController
          visible={showModal}
          imageType={imageType}
          content={message}
          redirectTo={redirectTo}
          isLoop={isLoop}
          closeModal={this.closeModal} />


        <VerificationPhoneScreen
          code={code}
          setCodeChangeHandler={this.setCodeChangeHandler}
          confirmCodeHandler={this.confirmCodeHandler}
          resendCodeHandler={this.resendCodeHandler}
        />
      </>
    )
  }
};

const mapStateToProps = state => ({
  auth: state.auth
})

export default connect(mapStateToProps, { signup })(VerificationPhoneController);