import React from 'react';
import {
  View, StyleSheet
} from 'react-native';
import { Item, Input, Button } from 'native-base';
import { RVText } from '../../core';
import { AuthBackgroundTwo } from '../../components';

const VerificationPhoneScreen = ({ code ,setCodeChangeHandler, confirmCodeHandler, resendCodeHandler }) => {

  return (
    <>
      <AuthBackgroundTwo isLeftIcon={true} leftIconColor="#fff">
        {/* <Form> */}
        <View style={{ flex: 1, paddingLeft: 15, paddingRight: 15,marginBottom:50 }}>
          <View style={{ alignItems: 'center', paddingTop: 40, paddingBottom: 20 }}>
            <RVText content='Verify Me' style={{ fontWeight: 'bold', fontSize: 20 }} />
          </View>
          <Item floatingLabel>
            <Input onChangeText={(e) => setCodeChangeHandler(e)}
              placeholder='Enter code from the sms'
              value={code}
              style={{ fontSize: 14, textAlign: 'center' }}
              keyboardType='numeric' />
          </Item>
          <Button block small rounded style={styles.authButton} onPress={confirmCodeHandler}>
            <RVText content={'Confirm'} style={{ fontSize: 14, color: '#fff' }} />
          </Button>
          <Button block small rounded style={[styles.authButton, { backgroundColor: '#00aaff' }]} onPress={resendCodeHandler}>
            <RVText content={'Resend SMS'} style={{ fontSize: 14, color: '#fff' }} />
          </Button>
          {/* <Button block small rounded
              style={[styles.authButton, { backgroundColor: '#00fe' }]}
              onPress={() => navigation.goBack()}>
              <RVText content={'Go Back'} style={{ fontSize: 14, color: '#fff' }} />
            </Button> */}
        </View>
        {/* </Form> */}
      </AuthBackgroundTwo>
    </>
  )
}

export default VerificationPhoneScreen;

const styles = StyleSheet.create({
  authButton: {
    marginTop: 20,
    justifyContent: "center",
    color: '#21216d',
    flexWrap: 'wrap'
  }
});
