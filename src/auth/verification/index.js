import VerificationPhoneController from './VerificationPhoneController';
import VerificationPhoneScreen from './VerificationPhoneScreen';

export {
    VerificationPhoneController,
    VerificationPhoneScreen
}